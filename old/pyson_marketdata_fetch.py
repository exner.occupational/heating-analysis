from util.const import MARKET_DATA
import requests
from datetime import date, timedelta
from util.epoch import get_epoch_from_date, get_datetime_from_epoch
import os
import sys
from pysondb.db import PysonDB
from pprint import pprint
import functools

url = "https://api.awattar.at/v1/marketdata"
today = date.today()
tomorrow = today + timedelta(days=1)
mydir = os.path.dirname(os.path.realpath(__file__))

def usage():
    print("usage:")
    print(f"{sys.argv[0]} [hist]")
    exit(1)

def get_batches(n: int = 500, date_from: date = date(2014, 1, 1)):
#def get_batches(n: int = 10, date_from: date = date(2022, 5, 1)):
    """
    return n-sized batches of timeframes of available aWATTar data (last batch may be smaller)
        batches: list of (<date from>, <date to>)
    """
    batches = []
    while (date_to := date_from + timedelta(days=n)) < tomorrow:
        batches.append((date_from, date_to))
        date_from = date_to
    if date_from != today:
        batches.append((date_from, today))
    return batches

def market_data_by_date(data, d):
    if data['type'] == MARKET_DATA and data['date'] == d.isoformat():
        return True

def check_batch(db, from_date, to_date):
    """
    check if all days in intervall are stored in the database
    """
    print(f"\ncheck_batch ({from_date.isoformat()} - {to_date.isoformat()})")
    data_points = 0
    numdays = (to_date - from_date).days
    for d in [from_date + timedelta(days=x) for x in range(numdays)]:
        #print(f"check_batch: process {d}")
        #Q = Query()
        #record = db.search((Q.type == MARKET_DATA) & (Q.date == d.isoformat()))
        market_data_by_date_partial = functools.partial(market_data_by_date, d=d)
        record = db.get_by_query(query=market_data_by_date_partial)
        if len(record) != 1:
            print(f"check_batch: found no datapoints")
            return False
        else:
            data_points += len(list(record.values())[0]["data"])
    print(f"check_batch: found {data_points} datapoints")
    return numdays * 24 == data_points

def process_batch(db, from_date, to_date):
    numdays = (to_date - from_date).days
    for d in [from_date + timedelta(days=x) for x in range(numdays)]:
        #print(f"fetch_batch: process {d}")
        market_data_dict = fetch_market_data(d)
        upsert_market_data(db, d, market_data_dict)

def fetch_market_data(from_date: date, to_date: date = None):
    start_epoch = get_epoch_from_date(from_date, vienna=True)
    if to_date is None:
        to_date = from_date + timedelta(days=1)
    end_epoch = get_epoch_from_date(to_date, vienna=True)
    print(f"fetch_market_data: from {ts_to_str(start_epoch)} / {start_epoch} to {ts_to_str(end_epoch)} / {end_epoch}")
    response = requests.get(f"{url}?start={start_epoch}&end={end_epoch}")
    sc = response.status_code
    if sc != 200:
        raise Exception("No response from aWATTar")
    return response.json()

def upsert_market_data(db, d: date, market_data_dict: dict):
    #print("upsert_market_data:")
    #print_marketdata(market_data_dict)
    #Q = Query()
    #db.upsert({'type': MARKET_DATA, 'date': d.isoformat(), 'data': market_data_dict["data"]},
    #    (Q.type == MARKET_DATA) & (Q.date == d.isoformat()))
    db.delete_by_query(lambda x: x['type'] == MARKET_DATA and x['date'] == d.isoformat())
    db.add({'type': MARKET_DATA, 'date': d.isoformat(), 'data': market_data_dict["data"]})
    db.commit()

def ts_to_str(ts):
    dt = get_datetime_from_epoch(ts, vienna=True)
    #if dt.minute != 0:
    #    raise Exception("minutes should be 0")
    return dt.strftime('%Y-%m-%d %H:%M:%S')

def eurmwh_to_centkwh(eurmwhfloat):
    return eurmwhfloat / 10

def print_marketdata(d: dict):
    for r in d["data"]:
        sts = r["start_timestamp"]
        ets = r["end_timestamp"]
        mp = r["marketprice"]
        print(f'{ts_to_str(sts)} / {sts} - {ts_to_str(ets)} / {ets}: {eurmwh_to_centkwh(mp)}')

def print_samples():
    for y in range(2013, 2016):
        for m in range (1, 12):
            d = date(y, m, 1)
            j = get_marketdata(d)
            print_marketdata(j)

if __name__ == "__main__":

    if len(sys.argv) == 2 and sys.argv[1] == "hist":
        hist = True
    elif len(sys.argv) == 1:
        hist = False
    else:
        usage()

    db = PysonDB(f"{mydir}/data/pyson_strom_db.json")

    # load month by month if necessary
    if hist:
        for b in get_batches():
            if not check_batch(db, *b):
                print(f"fetch & upsert{b}")
                process_batch(db, *b)
            else:
                print(f"ok {b}")
    # load next day
    else:
        #d = tomorrow
        d = today
        print(f"\nfetch tomorrows marketdata ({d.isoformat()})")
        market_data_dict = fetch_market_data(d)
        upsert_market_data(db, d, market_data_dict)
