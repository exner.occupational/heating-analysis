import os
from datetime import datetime

dir = os.path.dirname(os.path.realpath(__file__))
current_datetime = datetime.now()
ts_str = current_datetime.strftime("%Y-%m-%d %H:%M:%S")

with open(f"{dir}/test.txt", "a") as myfile:
    myfile.write(f"{ts_str}\n")
