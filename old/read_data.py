from datetime import datetime


filename_hc = "data/historical_consumption.csv"
filename_wd = "data/historical_weather_2014_2022_part2.csv"
filename_dh = "data/historical_weather_2014_2022_part3.csv"
# hc: sorted dict date -> zählerstand
# wd: sorted dict date -> sorted dict datetime -> (temperature, radiation, windspeed)
# dh: sorted dict date -> daylight_hours



# read consumption
# (strom) alex@NUC:~/lokal/git/strom/data$ head historical_consumption.csv 
# Datum,Nachtstrom (kWh)
# 2013-06-25,320915.3

with open(filename_hc) as file:
    file.readline() # skip header
    lines = [line.rstrip() for line in file]
    pairs = [line.split(",", 1) for line in lines]
    #hc = [(datetime.fromisoformat(k), float(v)) for k, v in pairs]
    #hc = dict(pairs)
    tmp = {datetime.fromisoformat(k): float(v) for k, v in pairs}
    hc = dict(sorted(tmp.items()))

# read weather data
# (strom) alex@NUC:~/lokal/git/strom/data$ head historical_weather_2014_2022_part2.csv
# time,temperature_2m (°C),shortwave_radiation (W/m²),windspeed_10m (m/s)
# 2014-01-01T00:00,3.2,0.0,4.53


# read daylight hours
# (strom) alex@NUC:~/lokal/git/strom/data$ head historical_weather_2014_2022_part3.csv
# time,sunrise (iso8601),sunset (iso8601)
# 2014-01-01,2014-01-01T07:43,2014-01-01T16:13

with open(filename_dh) as file:
    file.readline() # skip header
    lines = [line.rstrip() for line in file]
    tuples = [line.split(",", 2) for line in lines]
    tmp = {datetime.fromisoformat(k): (datetime.fromisoformat(sunset) - datetime.fromisoformat(sunrise)).seconds / 3600 for k, sunrise, sunset in tuples}
    dh = dict(sorted(tmp.items()))


