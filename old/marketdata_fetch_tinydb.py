from util.const import MARKET_DATA
import requests
from datetime import date, timedelta
from util.epoch import get_epoch_from_date, get_datetime_from_epoch, get_batches
import os
import sys
from tinydb import TinyDB, Query
from pprint import pprint

url = "https://api.awattar.at/v1/marketdata"
today = date.today()
tomorrow = today + timedelta(days=1)
mydir = os.path.dirname(os.path.realpath(__file__))

def usage():
    print("usage:")
    print(f"{sys.argv[0]} [hist]")
    exit(1)

def check_batch(db, date_from, date_to):
    """
    check if all days in intervall are stored in the database
    """
    print(f"\ncheck_batch ({date_from.isoformat()} - {date_to.isoformat()})")
    data_points = 0
    numdays = (date_to - date_from).days
    for d in [date_from + timedelta(days=x) for x in range(numdays)]:
        #print(f"check_batch: process {d}")
        Q = Query()
        record = db.search((Q.type == MARKET_DATA) & (Q.date == d.isoformat()))
        if len(record) != 1:
            print(f"check_batch: found no datapoints")
            return False
        else:
            data_points += len(record[0]["data"])
    print(f"check_batch: found {data_points} datapoints")
    return numdays * 24 == data_points

def process_batch(db, date_from, date_to):
    numdays = (date_to - date_from).days
    for d in [date_from + timedelta(days=x) for x in range(numdays)]:
        #print(f"fetch_batch: process {d}")
        market_data_dict = fetch_market_data(d)
        upsert_market_data(db, d, market_data_dict)

def fetch_market_data(date_from: date, date_to: date = None):
    start_epoch = get_epoch_from_date(date_from, vienna=True)
    if date_to is None:
        date_to = date_from + timedelta(days=1)
    end_epoch = get_epoch_from_date(date_to, vienna=True)
    print(f"fetch_market_data: from {ts_to_str(start_epoch)} / {start_epoch} to {ts_to_str(end_epoch)} / {end_epoch}")
    response = requests.get(f"{url}?start={start_epoch}&end={end_epoch}")
    sc = response.status_code
    if sc != 200:
        raise Exception("No response from aWATTar")
    return response.json()

def upsert_market_data(db, d: date, market_data_dict: dict):
    #print("upsert_market_data:")
    #print_marketdata(market_data_dict)
    Q = Query()
    db.upsert({'type': MARKET_DATA, 'date': d.isoformat(), 'data': market_data_dict["data"]},
        (Q.type == MARKET_DATA) & (Q.date == d.isoformat()))

def ts_to_str(ts):
    dt = get_datetime_from_epoch(ts, ms=True, vienna=True)
    #if dt.minute != 0:
    #    raise Exception("minutes should be 0")
    return dt.strftime('%Y-%m-%d %H:%M:%S')

def eurmwh_to_centkwh(eurmwhfloat):
    return eurmwhfloat / 10

def print_marketdata(d: dict):
    for r in d["data"]:
        sts = r["start_timestamp"]
        ets = r["end_timestamp"]
        mp = r["marketprice"]
        print(f'{ts_to_str(sts)} / {sts} - {ts_to_str(ets)} / {ets}: {eurmwh_to_centkwh(mp)}')

def print_samples():
    for y in range(2013, 2016):
        for m in range (1, 12):
            d = date(y, m, 1)
            j = get_marketdata(d)
            print_marketdata(j)

if __name__ == "__main__":

    if len(sys.argv) == 2 and sys.argv[1] == "hist":
        hist = True
    elif len(sys.argv) == 1:
        hist = False
    else:
        usage()

    db = TinyDB(f"{mydir}/data/strom_db.json")

    # load month by month if necessary
    if hist:
        for (date_from, date_to) in get_batches(n=50, date_from=date(2014, 1, 1), date_to=today):
            if not check_batch(db, date_from, date_to):
                print(f"fetch & upsert{(date_from, date_to)}")
                process_batch(db, date_from, date_to)
            else:
                print(f"ok {(date_from, date_to)}")
    # load next day
    else:
        d = tomorrow
        #d = today
        print(f"\nfetch tomorrows marketdata ({d.isoformat()})")
        market_data_dict = fetch_market_data(d)
        upsert_market_data(db, d, market_data_dict)
