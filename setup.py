#!/usr/bin/env python

from setuptools import setup, find_packages
from distutils.util import convert_path

version = open("strom/version.py").readline().split("=")[1].replace("'", "").strip()

requirements = [line.strip() for line in open("requirements.txt").readlines() if not line.strip().startswith(("#", "--"))]

setup(name='heating_analysis',
    version=version,
    description='Automatische Stromwahl',
    author='Alexander Exner',
    author_email='exner.occupational@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requirements,
    extras_require={'test': ['pytest', 'pytest-mock', 'Mock.GPIO'], },
)
