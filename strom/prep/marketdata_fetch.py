from util.const import MARKET_DATA
import requests
from datetime import date, timedelta
from util.epoch import get_epoch_from_date, get_datetime_from_epoch, get_batches
import os
import sys
from tinydb import TinyDB, Query
from pprint import pprint

url = "https://api.awattar.at/v1/marketdata"
today = date.today()
tomorrow = today + timedelta(days=1)
mydir = os.path.dirname(os.path.realpath(__file__))
file_hist = f"{mydir}/data/marketdata_hist.csv"
file_next = f"{mydir}/data/marketdata_next.csv"

def usage():
    print("usage:")
    print(f"{sys.argv[0]} [hist]")
    exit(1)

def process_batch(db, date_from, date_to):
    market_data_dict = fetch_market_data(date_from, date_to)
    print(market_data_dict)
    pass
    numdays = (date_to - date_from).days
    for d in [date_from + timedelta(days=x) for x in range(numdays)]:
        #print(f"fetch_batch: process {d}")
        upsert_market_data(db, d, market_data_dict)

def fetch_market_data(date_from: date, date_to: date):
    assert isinstance(date_from, date)
    assert isinstance(date_to, date)
    start_epoch = get_epoch_from_date(date_from, ms=True, vienna=True)
    end_epoch = get_epoch_from_date(date_to, ms=True, vienna=True)
    print(f"fetch_market_data: from {ts_to_str(start_epoch)} / {start_epoch} to {ts_to_str(end_epoch)} / {end_epoch}")
    response = requests.get(f"{url}?start={start_epoch}&end={end_epoch}")
    sc = response.status_code
    if sc != 200:
        raise Exception("No response from aWATTar")
    return response.json()

def ts_to_str(ts):
    dt = get_datetime_from_epoch(ts, ms=True, vienna=True)
    #if dt.minute != 0:
    #    raise Exception("minutes should be 0")
    return dt.strftime('%Y-%m-%d %H:%M:%S')

def eurmwh_to_centkwh(eurmwhfloat):
    return eurmwhfloat / 10

def print_marketdata(d: dict):
    for r in d["data"]:
        sts = r["start_timestamp"]
        ets = r["end_timestamp"]
        mp = r["marketprice"]
        print(f'{ts_to_str(sts)} / {sts} - {ts_to_str(ets)} / {ets}: {eurmwh_to_centkwh(mp)}')

def print_samples():
    for y in range(2013, 2016):
        for m in range (1, 12):
            d = date(y, m, 1)
            j = get_marketdata(d)
            print_marketdata(j)

def write_header(f):
    f.write("date,hour,start_timestamp,end_timestamp,marketprice,unit\n")

def write_data(f, market_data_dict: dict):
    for r in market_data_dict["data"]:
        sts = r["start_timestamp"]
        ets = r["end_timestamp"]
        d = get_datetime_from_epoch(sts, ms=True, vienna=True)
        mp = r["marketprice"]
        unit = r["unit"]
        f.write(f"{d.strftime('%Y-%m-%d')},{d.hour},{sts},{ets},{mp},{unit}\n")

if __name__ == "__main__":

    if len(sys.argv) == 2 and sys.argv[1] == "hist":
        hist = True
    elif len(sys.argv) == 1:
        hist = False
    else:
        usage()

    db = TinyDB(f"{mydir}/data/strom_db.json")

    # load month by month if necessary
    if hist:
        with open(file_hist, "w") as f:
            write_header(f)
            for (date_from, date_to) in get_batches(n=200, date_from=date(2014, 1, 1), date_to=tomorrow):
            #for (date_from, date_to) in get_batches(n=50, date_from=date(2014, 1, 1), date_to=date(2014, 1, 3)):
                print(f"fetch {date_from.isoformat()} - {date_to.isoformat()}")
                market_data_dict = fetch_market_data(date_from, date_to)
                write_data(f, market_data_dict)
    # load next day
    else:
        with open(file_next, "w") as f:
            write_header(f)
            #date_from = tomorrow
            date_from = today
            date_to = date_from + timedelta(days=1)
            print(f"\nfetch tomorrows marketdata ({date_from.isoformat()})")
            market_data_dict = fetch_market_data(date_from, date_to)
            write_data(f, market_data_dict)
            #upsert_market_data(db, d, market_data_dict)
