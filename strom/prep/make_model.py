from strom.util.const import LATITUDE, LONGITUDE, WEATHER_FORECAST
from strom.util.sunrise import Sun
from datetime import timedelta
import pandas as pd
import numpy as np
#from sklearn.linear_model import LinearRegression
from sklearn import linear_model
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error, r2_score
import statsmodels.api as sm # alternative to sklearn
import matplotlib.pyplot as plt


#
#alex@NUC:~/git/heating-analysis$ head -n 3 data/historical_weather_2023_2024.csv
#time,temperature,windspeed,radiation
#2023-03-29T00:00,1.1,9.0,0.0
#2023-03-29T01:00,0.8,9.9,0.0
#alex@NUC:~/git/heating-analysis$ head -n 3 data/NS-20230330-20240329.csv
#Datum,NS
#30.03.23,48.185
#31.03.23,38.181
#

filename_consumption = "data/NS-20230330-20240329.csv"
filename_weather_data = "data/historical_weather_2023_2024.csv"
summer_period = set([6, 7, 8])
winter_period = set([10, 11, 12, 1, 2, 3, 4])
#mysun = Sun(lat=LATITUDE, long=LONGITUDE)

def df_info(df):
    print(f"Dimension of dataset: {df.shape}\n")
    print(df.head()) # To get first n rows from the dataset default value of n is 5
    print()
    print(df.info())

def normalize(y):
    return ((y - y.min()) / (y.max() - y.min()), (y.min()), (y.max()))

def heating_period(d):
    """
    returns
        if day in winter period: w
        if day in summer period: s
        else: m
    """
    #month = set([d.month for d in pd.date_range(d1, d2 - timedelta(days=1), freq='d')])
    if d.month in winter_period:
        return "w"
    elif d.month in summer_period:
        return "s"
    else:
        return "m"

def sum_daylight_hours(date_from, date_to):
    sum = 0.0
    for d in pd.date_range(date_from, date_to - timedelta(days=1), freq='d'):
        #print(d)
        midnight = d.to_pydatetime().replace(hour=2, minute=0, second=0, microsecond=0)
        #print(midnight)
        mysun = Sun(when=midnight, lat=LATITUDE, long=LONGITUDE)
        sum += (mysun.sunset() - mysun.sunrise()).seconds / 3600
    return sum * 24 # workaround

def get_daylight_hours(d):
    mysun = Sun(when=d, lat=LATITUDE, long=LONGITUDE)
    return (mysun.sunset() - mysun.sunrise()).seconds / 3600


# read consumption
#alex@NUC:~/git/heating-analysis$ head -n 3 data/NS-20230330-20240329.csv
#Datum,NS
#30.03.23,48.185
#31.03.23,38.181
## (strom) alex@NUC:~/lokal/git/strom/data$ head historical_consumption.csv 
## date,meter
## 2013-06-25,320915.3

print("\nhistorical consumption")
#df = pd.read_csv(filename_consumption, index_col=0, parse_dates=True, dtype={'meter': np.float64})
#df = df.sort_index()
df = pd.read_csv(filename_consumption, dtype={'NS': np.float64})
df['date'] = pd.to_datetime(df['Datum'], format="%d.%m.%y")
df = df.sort_values('date')
df2 = pd.DataFrame()
df2 = pd.DataFrame({'date_from': pd.Series(dtype='datetime64[ns]'), 'date_to': pd.Series(dtype='datetime64[ns]'), 'heating_period': pd.Series(dtype='str')})

for i in range(0, len(df) - 1):
    df2.loc[i, 'consumption'] = df.loc[i, 'consumption']
    date = df.loc[i, 'date']
    #date_from = df.loc[i, 'date']
    #date_to = df.loc[i + 1, 'date']
    #df2.loc[i, 'date_from'] = date_from
    #df2.loc[i, 'date_to'] = date_to
    df2.loc[i, 'date'] = date
    df2.loc[i, 'heating_period'] = heating_period(date)
 
df_info(df2)
df_consumption = df2

# calculate summer consumption
df_consumption_s = df_consumption.loc[df_consumption["heating_period"] == "s"]
df_consumption_w = df_consumption.loc[df_consumption["heating_period"] == "w"]
summer_consumption = df_consumption_s["consumption"].sum() / len(df_consumption_s) # ToDo: Average?

# read weather data
#alex@NUC:~/git/heating-analysis$ head -n 3 data/historical_weather_2023_2024.csv
#time,temperature,windspeed,radiation
#2023-03-29T00:00,1.1,9.0,0.0
#2023-03-29T01:00,0.8,9.9,0.0
## (strom) alex@NUC:~/lokal/git/strom/data$ head historical_weather_2014_2022_part2.csv
## time,temperature,radiation,windspeed
## 2014-01-01T00:00,3.2,0.0,4.53

print("\nweather data")
df = pd.read_csv(filename_weather_data, dtype={'temperature': np.float64, 'windspeed': np.float64, 'radiation': np.float64})
df['time'] = pd.to_datetime(df['time'], format="%Y-%m-%dT%H:%M")
df = df.groupby(df.time.dt.date).sum(numeric_only=True) # groups data on dates and calculates the sum for all numeric columns of DataFrame
df.reset_index(inplace=True)
df['time'] = pd.to_datetime(df['time'], format="%Y-%m-%dT%H:%M")
df.set_index('time', inplace=True)
df.index.names = ['date']
df_info(df)
df_weather = df
#min_weather_date = df_weather.index[0]
#max_weather_date = df_weather.index[-1]

# read daylight hours
# (strom) alex@NUC:~/lokal/git/strom/data$ head historical_weather_2014_2022_part3.csv
# time,sunrise,sunset
# 2014-01-01,2014-01-01T07:43,2014-01-01T16:13


#######  linear regression ###

df_regression = df_consumption_w

# remove data out of range
#df_regression = df_regression[df_regression["date_from"] >= min_weather_date]
#df_regression = df_regression[df_regression["date_to"] <= max_weather_date]

#for row in df_regression.itertuples():
for index, row in df_regression.iterrows():
    #df_weather_sub = df_weather.loc[(df_weather.index >= row.date_from) & (df_weather.index < row.date_to)]
    df_weather_sub = df_weather.loc[row.date]
    #print("df_weather_sub:")
    #df_info(df_weather_sub)
    df_regression.loc[index, "temperature"] = df_weather_sub["temperature"].sum()
    #df_regression.loc[index, "radiation"] = df_weather_sub["radiation"].sum()
    df_regression.loc[index, "windspeed"] = df_weather_sub["windspeed"].sum()
    df_regression.loc[index, "daylight_hours"] = get_daylight_hours(row.date) * 24 # workaround because later divided by 24
#df_regression["daylight_hours"] = df_regression.apply(lambda x: sum_daylight_hours(x["date_from"], x["date_to"]), axis=1)

#variables = ["temperature", "radiation", "windspeed", "daylight_hours"]
variables = ["temperature", "windspeed", "daylight_hours"]

#convert to per day values
for v in variables:
    df_regression[v] = df_regression[v] / 24

#variables = ["radiation", "windspeed", "daylight_hours"]
#variables = ["temperature"]
#variables = ["radiation"]
#variables = ["windspeed"]
#variables = ["daylight_hours"]
X = df_regression[variables]
(X, minX, maxX) = normalize(X)
y = df_regression["consumption"]
#y = normalize(y)

print("\n" + "=" * 50)
print(f"summer consumption: {summer_consumption} kWh/day")
print("=" * 50 + "\n")

# with sklearn
regressor = linear_model.LinearRegression()
regressor.fit(X, y)
# get sklearn.metrics
X_test = X # dataset should have been split in two parts, but too less records...
y_test = y # dataset should have been split in two parts, but too less records...
pred = regressor.predict(X_test)
test_set_rmse = (np.sqrt(mean_squared_error(y_test, pred)))
test_set_r2 = r2_score(y_test, pred)
print(f"const: {regressor.intercept_}")
print(f"coeff: {regressor.coef_}")
print(f"rmse:  {test_set_rmse}")
print(f"r-squ: {test_set_r2}")
model_dict = {v: regressor.coef_[i] for i, v in enumerate(variables)}
model_dict["const"] = regressor.intercept_
model_dict["min"] = minX.to_dict()
model_dict["max"] = maxX.to_dict()
print(model_dict)


# with statsmodels
x = sm.add_constant(X) # adding a constant
model = sm.OLS(y, x).fit()
predictions = model.predict(x) 
print_model = model.summary()
print(print_model)

# plot chart if possible
if len(variables) == 1:
    xfit = np.linspace(0, 1, 10)
    yfit = regressor.predict(xfit[:, np.newaxis])
    plt.scatter(X, y);
    plt.plot(xfit, yfit)
    plt.show()
