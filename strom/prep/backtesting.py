from util.epoch import get_datetime_from_epoch, get_epoch_from_datetime, get_epoch_from_date
from util.const import VIENNA
from util.daylength import daylength
from forecast import heating_time, eval_model
from datetime import date, datetime, timedelta
import pandas as pd
import numpy as np

file_weather_data = "data/historical_weather_2014_2022_part2.csv"
file_market_data = "data/marketdata_hist.csv"
file_backtest = "data/backtest.csv"

month_summer = [5, 6, 7, 8, 9]
date_from = date(2014, 1, 1)
date_to = date(2022, 12, 15)
date_diff = date_to - date_from # returns timedelta


if __name__ == "__main__":

    # Alle Wetterdaten einlesen
    # time,temperature,radiation,windspeed
    # 1388534400,3.4,0.0,16.1
    df_w = pd.read_csv(file_weather_data, dtype={'time': np.int32, 'temperature': np.float64, 'windspeed': np.float64, 'radiation': np.float64})
    df_w['datetime'] = pd.to_datetime(df_w['time'], unit="s") # Unix epoch in datetime umwandeln
    # Alle Marktdaten einlesen
    # date,hour,start_timestamp,end_timestamp,marketprice,unit
    # 2014-01-01,0,1388530800000,1388534400000,15.15,Eur/MWh
    df_m = pd.read_csv(file_market_data, dtype={'market_price': np.float64})
    #df_m['datetime'] = pd.to_datetime(df_m['date']) # string in datetime umwandeln

    with open(file_backtest, "w") as f:
        f.write(f"date,epoch_from,epoch_to,heating_time,kwh,avg_cost\n")

        # Über jeden Tag iterieren
        for i in range(date_diff.days + 1):
            d = date_from + timedelta(days=i)
            epoch_from = get_epoch_from_date(d, ms=False, vienna=True)
            datetime_from = datetime(d.year, d.month, d.day, 0, 0, 0, 0, tzinfo=VIENNA)
            epoch_to = get_epoch_from_datetime(datetime_from + timedelta(days=1, hours=-1), ms=False)

            # Einen Tag holen
            df_w_sub = df_w[df_w['time'].between(epoch_from, epoch_to)]
            df_m_sub = df_m[df_m['date'] == d.strftime('%Y-%m-%d')]
            #print(f"{len(df_w_sub.index)} / {len(df_m_sub.index)}") # 24 records
            # add all rows of numeric fields except time, divide by 24 and transform to dict

            # Input (param_dict) für Modell erzeugen
            param_dict = (df_w_sub.drop('time', axis=1).sum(numeric_only=True) / 24).to_dict()
            param_dict["daylight_hours"] = daylength(d.timetuple().tm_yday)

            # Modell evaluieren
            kwh = eval_model(param_dict, d.month in month_summer)

            # Liste von Marktpreisen [(<hour>, <price>), ...]
            market_data = [[x[2], x[5]] for x in list(df_m_sub.to_records())]

            # get list of hours
            (ht, avg_cost) = heating_time(d, kwh, market_data)
            #print(ht)
            #ct_per_h = mean_cost(ht)
            #print_d = {k: int(v) for k, v in param_dict.items()}
            #print(f"param_dict: {print_d}")
            f.write(f"{d},{epoch_from},{epoch_to},{len(ht)},{kwh},{avg_cost}\n")
        

