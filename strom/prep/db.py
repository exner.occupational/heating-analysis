from datetime import datetime, date, timedelta
from util.const import VIENNA, UTC
from util.epoch import get_epoch_from_datetime
import os
import sys
from tinydb import TinyDB, Query
import json
from pprint import pprint

def usage():
    print("usage:")
    print(f"{sys.argv[0]} [prod]")
    exit(1)

if len(sys.argv) == 2 and sys.argv[1] == "prod":
    db_file = "/home/alex/Öffentlich/Computer_IKT/Python/data/strom_db.json"
elif len(sys.argv) == 1:
    dir = os.path.dirname(os.path.realpath(__file__))
    db_file = f"{dir}/data/strom_db.json"
else:
    usage()

db = TinyDB(db_file)
X = Query()
print("connected to database {db_file}")
print("see https://tinydb.readthedocs.io/en/latest/getting-started.html")
print("""
Basic Usage

>>> from tinydb import TinyDB, Query
>>> db = TinyDB('db.json')
>>> db.insert({'type': 'apple', 'count': 7})
>>> db.insert({'type': 'peach', 'count': 3})
Now you can get all documents stored in the database by running:

>>> db.all()
[{'count': 7, 'type': 'apple'}, {'count': 3, 'type': 'peach'}]
You can also iter over stored documents:

>>> for item in db:
>>>     print(item)
{'count': 7, 'type': 'apple'}
{'count': 3, 'type': 'peach'}
Of course you’ll also want to search for specific documents. Let’s try:

>>> Fruit = Query()
>>> db.search(Fruit.type == 'peach')
[{'count': 3, 'type': 'peach'}]
>>> db.search(Fruit.count > 5)
[{'count': 7, 'type': 'apple'}]
Next we’ll update the count field of the apples:

>>> db.update({'count': 10}, Fruit.type == 'apple')
>>> db.all()
[{'count': 10, 'type': 'apple'}, {'count': 3, 'type': 'peach'}]
In the same manner you can also remove documents:

>>> db.remove(Fruit.count < 5)
>>> db.all()
[{'count': 10, 'type': 'apple'}]
And of course you can throw away all data to start with an empty database:

>>> db.truncate()
>>> db.all()
[]

###

db.search((X.type=='weather_forecast') & (X.date=='2022-12-29'))
db.search((X.type=='market_data') & (X.date=='2022-12-29'))
""")
