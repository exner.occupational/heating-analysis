from util.const import LATITUDE, LONGITUDE, WEATHER_FORECAST
from util.sunrise import sun
from datetime import timedelta
import pandas as pd
import numpy as np
#from sklearn.linear_model import LinearRegression
from sklearn import linear_model
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error, r2_score
import statsmodels.api as sm # alternative to sklearn
import matplotlib.pyplot as plt


filename_hc = "data/historical_consumption.csv"
filename_weather_data = "data/historical_weather_2014_2022_part2.csv"
no_winter_period = set([6, 7, 8])
no_summer_period = set([10, 11, 12, 1, 2, 3, 4])
mysun = sun(lat=LATITUDE, long=LONGITUDE)

def df_info(df):
    print(f"Dimension of dataset: {df.shape}\n")
    print(df.head()) # To get first n rows from the dataset default value of n is 5
    print()
    print(df.info())

def normalize(y):
    return ((y - y.min()) / (y.max() - y.min()), (y.min()), (y.max()))

def heating_period(d1, d2):
    """
    returns
        if all days in heating period: w
        if all days out of heating period: s
        else: m (mixed)
    """
    month = set([d.month for d in pd.date_range(d1, d2 - timedelta(days=1), freq='d')])
    if len(month.intersection(no_winter_period)) == 0:
        return "w"
    elif len(month.intersection(no_summer_period)) == 0:
        return "s"
    else:
        return "m"

def sum_daylight_hours(date_from, date_to):
    sum = 0.0
    for d in pd.date_range(date_from, date_to - timedelta(days=1), freq='d'):
        #print(d)
        midnight = d.to_pydatetime().replace(hour=2, minute=0, second=0, microsecond=0)
        #print(midnight)
        sum += (mysun.sunset(when=midnight) - mysun.sunrise(when=midnight)).seconds / 3600
    return sum * 24 # workaround


# read consumption
# (strom) alex@NUC:~/lokal/git/strom/data$ head historical_consumption.csv 
# date,meter
# 2013-06-25,320915.3

print("\nhistorical consumption")
#df = pd.read_csv(filename_hc, index_col=0, parse_dates=True, dtype={'meter': np.float64})
#df = df.sort_index()
df = pd.read_csv(filename_hc, dtype={'meter': np.float64})
df['date'] = pd.to_datetime(df['date'])
df = df.sort_values('date')
df2 = pd.DataFrame()
for i in range(0, len(df) - 1):
    df2.loc[i, 'consumption'] = df.loc[i + 1, 'meter'] - df.loc[i, 'meter']
    date_from = df.loc[i, 'date']
    date_to = df.loc[i + 1, 'date']
    df2.loc[i, 'date_from'] = date_from
    df2.loc[i, 'date_to'] = date_to
    df2.loc[i, 'heating_period'] = heating_period(date_from, date_to)
df2['diff_days'] = (df2['date_to'] - df2['date_from']) / np.timedelta64(1, 'D')
 
df_info(df2)
df_consumption = df2

# calculate summer consumption
df_consumption_s = df_consumption.loc[df_consumption["heating_period"] == "s"]
df_consumption_w = df_consumption.loc[df_consumption["heating_period"] == "w"]
summer_consumption = df_consumption_s["consumption"].sum() / df_consumption_s["diff_days"].sum()

# read weather data
# (strom) alex@NUC:~/lokal/git/strom/data$ head historical_weather_2014_2022_part2.csv
# time,temperature,radiation,windspeed
# 2014-01-01T00:00,3.2,0.0,4.53

print("\nweather data")
df = pd.read_csv(filename_weather_data, dtype={'temperature': np.float64, 'radiation': np.float64, 'windspeed': np.float64})
df['time'] = pd.to_datetime(df['time'], unit="s")
df = df.groupby(df.time.dt.date).sum(numeric_only=True) # groups data on dates and calculates the sum for all numeric columns of DataFrame
df.reset_index(inplace=True)
df['time'] = pd.to_datetime(df['time'])
df.set_index('time', inplace=True)
df.index.names = ['date']
#df = df.set_index('time')
df_info(df)
df_weather = df
min_weather_date = df_weather.index[0]
max_weather_date = df_weather.index[-1]

# read daylight hours
# (strom) alex@NUC:~/lokal/git/strom/data$ head historical_weather_2014_2022_part3.csv
# time,sunrise,sunset
# 2014-01-01,2014-01-01T07:43,2014-01-01T16:13


#######  linear regression ###

df_regression = df_consumption_w

# remove data out of range
df_regression = df_regression[df_regression["date_from"] >= min_weather_date]
df_regression = df_regression[df_regression["date_to"] <= max_weather_date]

#for row in df_regression.itertuples():
for index, row in df_regression.iterrows():
    df_weather_sub = df_weather.loc[(df_weather.index >= row.date_from) & (df_weather.index < row.date_to)]
    df_regression.loc[index, "temperature"] = df_weather_sub["temperature"].sum()
    df_regression.loc[index, "radiation"] = df_weather_sub["radiation"].sum()
    df_regression.loc[index, "windspeed"] = df_weather_sub["windspeed"].sum()
df_regression["daylight_hours"] = df_regression.apply(lambda x: sum_daylight_hours(x["date_from"], x["date_to"]), axis=1)

variables = ["temperature", "radiation", "windspeed", "daylight_hours"]

#convert to per day values
for v in variables:
    df_regression[v] = df_regression[v] / df_regression["diff_days"] / 24
df_regression["consumption"] = df_regression["consumption"] / df_regression["diff_days"]

#variables = ["radiation", "windspeed", "daylight_hours"]
#variables = ["temperature"]
#variables = ["radiation"]
#variables = ["windspeed"]
#variables = ["daylight_hours"]
X = df_regression[variables]
(X, minX, maxX) = normalize(X)
y = df_regression["consumption"]
#y = normalize(y)

print("\n" + "=" * 50)
print(f"summer consumption: {summer_consumption} kWh/day")
print("=" * 50 + "\n")

# with sklearn
regressor = linear_model.LinearRegression()
regressor.fit(X, y)
# get sklearn.metrics
X_test = X # dataset should have been split in two parts, but too less records...
y_test = y # dataset should have been split in two parts, but too less records...
pred = regressor.predict(X_test)
test_set_rmse = (np.sqrt(mean_squared_error(y_test, pred)))
test_set_r2 = r2_score(y_test, pred)
print(f"const: {regressor.intercept_}")
print(f"coeff: {regressor.coef_}")
print(f"rmse:  {test_set_rmse}")
print(f"r-squ: {test_set_r2}")
model_dict = {v: regressor.coef_[i] for i, v in enumerate(variables)}
model_dict["const"] = regressor.intercept_
model_dict["min"] = minX.to_dict()
model_dict["max"] = maxX.to_dict()
print(model_dict)


# with statsmodels
x = sm.add_constant(X) # adding a constant
model = sm.OLS(y, x).fit()
predictions = model.predict(x) 
print_model = model.summary()
print(print_model)

# plot chart if possible
if len(variables) == 1:
    xfit = np.linspace(0, 1, 10)
    yfit = regressor.predict(xfit[:, np.newaxis])
    plt.scatter(X, y);
    plt.plot(xfit, yfit)
    plt.show()
