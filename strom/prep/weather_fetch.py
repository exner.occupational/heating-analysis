from util.const import LATITUDE, LONGITUDE, WEATHER_FORECAST, WEATHER_HISTORY 
from util.epoch import get_datetime_from_epoch, get_epoch_from_datetime, get_batches
from util.sunrise import sun
from datetime import datetime, date, timedelta
import requests
import os
import sys
from tinydb import TinyDB, Query
import json
from pprint import pprint

url_forecast = f"https://api.open-meteo.com/v1/forecast?latitude={LATITUDE}&longitude={LONGITUDE}&hourly=temperature_2m,windspeed_10m,shortwave_radiation&timeformat=unixtime&timezone=Europe%2FBerlin"
url_history = f"https://archive-api.open-meteo.com/v1/era5?latitude={LATITUDE}&longitude={LONGITUDE}&hourly=temperature_2m,windspeed_10m,shortwave_radiation&timeformat=unixtime&timezone=Europe%2FBerlin"
hourly_fields = ["time", "temperature_2m", "windspeed_10m", "shortwave_radiation"]
today = date.today()
tomorrow = today + timedelta(days=1)
mydir = os.path.dirname(os.path.realpath(__file__))
mysun = sun(lat=LATITUDE, long=LONGITUDE)

def usage():
    print("usage:")
    print(f"{sys.argv[0]} [hist]")
    exit(1)

def check_batch(db, date_from, date_to):
    """
    check if all days in intervall are stored in the database
    """
    print(f"\ncheck_batch ({date_from.isoformat()} - {date_to.isoformat()})")
    data_points = 0
    numdays = (date_to - date_from).days
    for d in [date_from + timedelta(days=x) for x in range(numdays)]:
        #print(f"check_batch: process {d}")
        Q = Query()
        record = db.search((Q.type == WEATHER_FORECAST) & (Q.date == d.isoformat()))
        if len(record) != 1:
            print(f"check_batch: found no datapoints")
            return False
        else:
            data_points += len(record[0]["hourly"]["time"])
    print(f"check_batch: found {data_points} datapoints")
    return numdays * 24 == data_points

def fetch_weather_forecast_data(hist: bool, date_from: date, date_to: date = None):
    if date_to is None:
        date_to = date_from + timedelta(days=1)
    url = url_history if hist else url_forecast

    from_isodate = date_from.isoformat()
    to_isodate = date_to.isoformat()
    response = requests.get(f"{url}&start_date={from_isodate}&end_date={to_isodate}")
    sc = response.status_code
    print(f"{from_isodate} - {to_isodate}: Status Code {sc}")
    if sc != 200:
        raise Exception("Got no weather forecast")
    j = response.json()
    return j

def upsert_weather_data(db, hist: bool, weather_forecast_dict):
    weather_type = WEATHER_HISTORY if hist else WEATHER_FORECASE
    a = []
    # build list of [iso-date, epoch, temperature_2m, windspeed_10m, shortwave_radiation]
    for i in range(len(weather_forecast_dict["hourly"]["time"])):
        b = [weather_forecast_dict["hourly"][f][i] for f in hourly_fields]
        b.append(get_datetime_from_epoch(b[0], ms=False, vienna=True))
        b.append(b[-1].date().isoformat())
        a.append(b)

    def get_epoch(b):
        return [1]

    a.sort(key=get_epoch) # for sure sort by time

    #pprint(a)
    # iterate over all dates
    for diso in set([b[-1] for b in a]):
        print(diso)
        aa = [b for b in a if b[-1] == diso] # get all records of date diso
        if len(aa) == 24:
            midnight = aa[0][-2].replace(hour=2, minute=0, second=0, microsecond=0) # set time to 2:00 at some date
            daily = {"sunrise": get_epoch_from_datetime(mysun.sunrise(when=midnight)),
                "sunset": get_epoch_from_datetime(mysun.sunset(when=midnight))}
            #pprint(aa)
            hourly = {f: [] for f in hourly_fields}
            for b in aa:
                for i, f in enumerate(hourly_fields):
                    hourly[f].append(b[i])
            #pprint(hourly)
            Q = Query()
            db.upsert({'type': weather_type, 'date': diso, 'daily': daily, 'hourly': hourly},
                (Q.type == weather_type) & (Q.date == diso))
        else:
            print(f"Error on processing date {diso}")
            print("\n" + "="*50 + "\n")
            pprint(len(aa))
            print("\n" + "X"*50 + "\n")
            pprint([(x[-1], x[0]) for x in a])
            print("\n" + "="*50 + "\n")

if __name__ == "__main__":

    if len(sys.argv) == 2 and sys.argv[1] == "hist":
        hist = True
    elif len(sys.argv) == 1:
        hist = False
    else:
        usage()

    db = TinyDB(f"{mydir}/data/strom_db.json")

    if hist:
        #for (date_from, date_to) in get_batches(n=50, date_from=date(2014, 1, 1), date_to=today):
        for (date_from, date_to) in get_batches(n=5, date_from=date(2014, 4, 11), date_to=today):
            if not check_batch(db, date_from, date_to):
                print(f"fetch & upsert{(date_from, date_to)}")
                weather_forecast_dict = fetch_weather_forecast_data(hist, date_from, date_to)
                upsert_weather_data(db, hist, weather_forecast_dict)
            else:
                print(f"ok {(date_from, date_to)}")
    else:
        date_from = tomorrow
        date_to = tomorrow + timedelta(days=2)
        print(f"\nfetch weather forecast from {from_date.isoformat()} - {to_date.isoformat()}")
        weather_forecast_dict = fetch_weather_forecast_data(hist, date_from, date_to)
        upsert_weather_data(db, hist, weather_forecast_dict)
