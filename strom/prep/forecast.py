from datetime import datetime, date, timedelta
from operator import itemgetter
import requests
import json
from strom.util.calc import calc_distribution

model_dict = {'temperature': -77.20352511166335, 'radiation': 2.7808968681494264, 'windspeed': 5.578920054492248, 'daylight_hours': -11.8095589532916, 'const': 115.56444552270315, 'min': {'temperature': -1.4917317708333335, 'radiation': 41.608539094650205, 'windspeed': 8.141008771929824, 'daylight_hours': 8.49654761904762}, 'max': {'temperature': 15.310912698412695, 'radiation': 228.96929824561403, 'windspeed': 16.41022012578616, 'daylight_hours': 13.581228070175436}}
month_winter = [9, 10, 11, 12, 1, 2, 3, 4, 5]
tomorrow = datetime.today() + timedelta(days=1)
tomorrow_iso = tomorrow.date().isoformat()
param_dict_keys = ["temperature", "radiation", "windspeed", "daylight_hours"]



def get_dict(subj):
    # pass e. g. hourly or daily from open-meteo response
    d = {}
    if not "time" in subj.keys():
        raise Exception("to time field available")
    for i, time in enumerate(subj["time"]):
        subd = {}
        for key in set(subj) - {"time"}:
            subd[key] = subj[key][i]
        d[time] = subd
    return d

def heating_time(d: date, kwh: int, market_data: list) -> (list, float):
    """
    d      .. date
    kwh    .. needed power
    prices .. list of prices to select, e. g. [(0, 3.14), (1, 5.33), (2, 7.55), (3, 9.77), (4, 4.56), ...]
    heating_period:   0 kWh: 0h
    heating_period: 120 kWh: 6h
    additional 1h in any case (cellar, hot water)

    return: list of hours, e. g [0, 1, 2, 3, 16, 17] and mean cost
    """
    assert isinstance(d, date)
    winter = True if d.month in month_winter else False
    heating_time = int(max(kwh * 0.05, 0) + 1 + 0.5) if winter else 1 # winter + boiler + zur kaufmännischen rundung
    heating_time = max(heating_time, 1)
    heating_time = min(heating_time, 10)
    distribution = calc_distribution(heating_time, market_data)
    #print(distribution)
    avg_cost = sum(x[1] for x in distribution) / len(distribution)
    return ([x[0] for x in distribution], avg_cost)

def eval_model(param_dict, summer_season:bool = False):
    """
    param_dict: {"temperature": <C>, "radiation": <W/m2> ,"windspeed": <km/h>, "daylight_hours": <h>}
    return: kWh
    """
    #print(f"param_dict: {param_dict}")
    # normalization
    normalized_dict = {}
    for f in param_dict_keys:
        normalized_dict[f] = (param_dict[f] - model_dict["min"][f]) / (model_dict["max"][f] - model_dict["min"][f])

    kwh = model_dict["temperature"] * normalized_dict["temperature"] + \
          model_dict["radiation"] * normalized_dict["radiation"] + \
          model_dict["windspeed"] * normalized_dict["windspeed"] + \
          model_dict["daylight_hours"] * normalized_dict["daylight_hours"] + \
          model_dict["const"]
    #print(f"kWh: {kwh}")

    kwh = 12 if summer_season else kwh # Annahme: Warmwasser und Keller benötigen 12 kWh pro Tag Winter wie Sommer
    return kwh

def get_param_tomorrow():
    """
    :return: input parameter of model. e. g.
        {'temperature': 257.25, 'radiation': 3440.0, 'windspeed': 374.8, 'daylight_hours': 11.55}
    """
    #url = "https://api.open-meteo.com/v1/forecast?latitude=48.1742&longitude=16.2559&hourly=temperature_2m,windspeed_10m,shortwave_radiation&daily=sunrise,sunset&timezone=Europe%2FBerlin&start_date=2022-11-29&end_date=2022-11-29"
    url = "https://api.open-meteo.com/v1/forecast?latitude=48.1742&longitude=16.2559&hourly=temperature_2m,windspeed_10m,shortwave_radiation&daily=sunrise,sunset&timezone=Europe%2FBerlin"
    response = requests.get(f"{url}&start_date={tomorrow_iso}&end_date={tomorrow_iso}")
    sc = response.status_code
    print(f"date: {tomorrow_iso}: Status Code {sc}")
    if sc != 200:
        raise Exception("Got no weather forecast")
    j = response.json()
    #parsed = json.loads(j)

    print(json.dumps(j["hourly"], indent=4))
    hourly_fields = ["temperature_2m", "shortwave_radiation", "windspeed_10m"]
    my_keys = ["temperature", "radiation", "windspeed"]
    param_dict = {f: 0.0 for f in param_dict_keys}
    hourly = get_dict(j["hourly"])
    h = 0
    for k, v in {key: value for key, value in hourly.items() if key.startswith(tomorrow_iso)}.items():
        h += 1
        for i, f in enumerate(hourly_fields):
            param_dict[my_keys[i]] = param_dict[my_keys[i]] + v[f]
    if h != 24:
        raise Exception("24 records expected but got {h}")

    #print(json.dumps(j["daily"], indent=4))
    daily = get_dict(j["daily"])
    sunrise = datetime.fromisoformat(daily[tomorrow_iso]["sunrise"])
    sunset = datetime.fromisoformat(daily[tomorrow_iso]["sunset"])
    daylight_hours = (sunset - sunrise).total_seconds() / 3600
    param_dict["daylight_hours"] = daylight_hours
    #print(f"daylight_hours: {daylight_hours}")
    return param_dict


if __name__ == "__main__":

    print(f"\ntomorrow")
    param_dict = get_param_tomorrow()
    kwh = eval_model(param_dict)

    print(f"\n-10 Grad mit Wind ohne Sonne")
    eval_model({'radiation': 0, 'windspeed': 30, 'temperature': -10, 'daylight_hours': 8.45})

    print(f"\n-10 Grad ohne Wind mit Sonne")
    eval_model({'radiation': 80, 'windspeed': 0, 'temperature': -10, 'daylight_hours': 8.45})

    print(f"\n0 Grad mit Wind ohne Sonne")
    eval_model({'radiation': 0, 'windspeed': 30, 'temperature': 0, 'daylight_hours': 8.45})

    print(f"\n0 Grad ohne Wind mit Sonne")
    eval_model({'radiation': 80, 'windspeed': 0, 'temperature': 0, 'daylight_hours': 8.45})

    print(f"\n10 Grad mit Wind ohne Sonne")
    eval_model({'radiation': 20, 'windspeed': 30, 'temperature': 10, 'daylight_hours': 8.45})

    print(f"\n10 Grad ohne Wind mit Sonne")
    eval_model({'radiation': 80, 'windspeed': 0, 'temperature': 10, 'daylight_hours': 8.45})

    print(f"\n20 Grad mit Wind ohne Sonne")
    eval_model({'radiation': 20, 'windspeed': 30, 'temperature': 20, 'daylight_hours': 8.45})

    print(f"\n20 Grad ohne Wind mit Sonne")
    eval_model({'radiation': 80, 'windspeed': 0, 'temperature': 20, 'daylight_hours': 8.45})

    '''
    consumption  date_from    date_to heating_period  diff_days  temperature  radiation  windspeed  daylight_hours
    1060.2 2019-10-12 2019-10-31              w       19.0       5273.6    43026.0    1031.47      202.001389
    3048.6 2019-10-31 2019-12-06              w       36.0       5183.8    40773.0    3410.63      333.302778
     763.3 2019-12-06 2019-12-13              w        7.0        151.1     7321.0     445.50       59.475833
    4662.2 2019-12-13 2020-01-29              w       47.0       1829.5    49766.0    3307.56      404.069722
    '''
    #print()
    #for x in [
    #        {'consumption': 1060, 'diff_days': 19, 'radiation': 43026, 'windspeed': 1031, 'temperature': 5273, 'daylight_hours': 202},
    #        {'consumption': 3048, 'diff_days': 36, 'radiation': 40773, 'windspeed': 3410, 'temperature': 5183, 'daylight_hours': 333},
    #        {'consumption': 763,  'diff_days': 7,  'radiation': 7321,  'windspeed': 445,  'temperature': 151,  'daylight_hours': 59},
    #        {'consumption': 4662, 'diff_days': 47, 'radiation': 49766, 'windspeed': 3307, 'temperature': 1829, 'daylight_hours': 404}]:
    #    diff_days = x["diff_days"]
    #    y = {k: int(v / diff_days) for k, v in x.items()}
    #    print(y)
    #    eval_model(y)
    #    print()
