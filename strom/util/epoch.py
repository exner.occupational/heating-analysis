from .const import VIENNA, UTC
from datetime import datetime, date, timedelta


# defining the timezone
#tz = pytz.timezone('Europe/Vienna')

def vienna_now():
    return datetime.now(VIENNA)

def get_midnight(dt: datetime):
    return dt.replace(hour=0, minute=0, second=0, microsecond=0)

def get_epoch_from_datetime(dt: datetime, ms: bool):
    if dt.tzinfo is None:
        raise Exception("Only timezone aware allowed")
    return int(dt.timestamp()) * (1000 if ms else 1)

def get_epoch_from_date(d: date, ms: bool, vienna: bool = False):
    tz = VIENNA if vienna else UTC
    dt = datetime(d.year, d.month, d.day, 0, 0, 0, tzinfo=tz)
    return int(dt.timestamp()) * (1000 if ms else 1)

def get_datetime_from_epoch(epoch: int, ms: bool, vienna: bool = False):
    tz = VIENNA if vienna else UTC
    epoch = epoch / 1000 if ms else epoch
    return datetime.fromtimestamp(epoch, tz)
    #dt = datetime.utcfromtimestamp(int(epoch / 1000))
    #dt = dt.astimezone(pytz.timezone('Europe/Vienna'))
    #return dt

#def get_date_from_epoch(epoch: int, ms: bool, vienna: bool = False):
#    dt = get_datetime_from_epoch(epoch, ms, vienna)
#    return dt.date()

def get_batches(n: int, date_from, date_to: date):
    """
    return n-sized batches of timeframes between date_fron amd date_to (last batch may be smaller)
        batches: list of (<date from>, <date to>)
    """
    assert isinstance(date_from, date)
    assert isinstance(date_to, date)
    batches = []
    while (date_next := date_from + timedelta(days=n)) <= date_to:
        batches.append((date_from, date_next))
        date_from = date_next
    if date_from != date_to:
        batches.append((date_from, date_to))
    return batches

def assert_epoch(epoch: int, ms: bool):
    """
    Plausibility check whether epoch is valid.
    """
    assert 1000000000 * (1000 if ms else 1) < epoch < 9999999999 * (1000 if ms else 1)

if __name__ == "__main__":
    for y in range(2015, 2018):
        for m in range (1, 12):
            dt = datetime(y, m, 1, 0, 0, 0, tzinfo=pytz.UTC)
            print(f"1.{m}.{y}: {get_epoch_from_datetime(dt)}")
