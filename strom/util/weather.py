from datetime import datetime, date, timedelta
import requests
from loguru import logger
from typing import Dict
from typeguard import typechecked
from strom.util.const import VIENNA
#import json

def get_dict(subj):
    """
    :return: e. g. on hourly sub-json:
        {'2023-03-10T00:00': {'shortwave_radiation': 0.0, 'windspeed_10m': 7.0, 'temperature_2m': 7.8},
         '2023-03-10T01:00': {'shortwave_radiation': 0.0, 'windspeed_10m': 8.2, 'temperature_2m': 8.7},
         '2023-03-10T02:00': {'shortwave_radiation': 0.0, 'windspeed_10m': 5.6, 'temperature_2m': 7.7},
         ...}
         e. g. on daily sub-json:
         {'2023-03-10': {'sunset': '2023-03-10T17:53', 'sunrise': '2023-03-10T06:17'}}
    """
    # pass e. g. hourly or daily from open-meteo response
    d = {}
    if not "time" in subj.keys():
        raise Exception("to time field available")
    for i, time in enumerate(subj["time"]):
        subd = {}
        for key in set(subj) - {"time"}:
            subd[key] = subj[key][i]
        d[time] = subd
    return d

@typechecked
def request_weather_data(start_d: date, end_d: date):
    """
    build an request like
    https://api.open-meteo.com/v1/forecast?latitude=48.1742&longitude=16.2559&hourly=temperature_2m,windspeed_10m,shortwave_radiation&daily=sunrise,sunset&timezone=Europe%2FBerlin&start_date=2022-11-29&end_date=2022-11-29
    and return raw json API response
    DOES FIX tz unaware datetime fields to tz aware datetime
    :return: e. g.:
    {'latitude': 48.18, ...,
     'hourly_units': {'time': 'iso8601', ...},
     'hourly': {'time': ['2023-03-10T00:00', ...], 'temperature_2m': [7.8, ...], 'windspeed_10m': [7.0, ...], 'shortwave_radiation': [0.0, ...]},
     'daily_units': {'time': 'iso8601', ...},
     'daily': {'time': ['2023-03-10'], 'sunrise': ['2023-03-10T06:17'], 'sunset': ['2023-03-10T17:53']}}
    """
    start_d_iso = start_d.isoformat()
    end_d_iso = end_d.isoformat()
    params = {
        "latitude": "48.1742",
        "longitude": "16.2559",
        "hourly": ["temperature_2m", "windspeed_10m", "shortwave_radiation"],
        "daily": ["sunrise", "sunset"],
        "timezone": "Europe/Berlin",
        "start_date": start_d_iso,
        "end_date": end_d_iso
    }
    url = "https://api.open-meteo.com/v1/forecast"
    response = requests.get(url, params=params)
    sc = response.status_code
    logger.debug(f"{start_d_iso} - {end_d_iso}: Status Code {sc}")
    if sc != 200:
        # ToDo: return fallback from model without weather data
        raise Exception("Got no weather forecast")
    j = response.json()
    j["hourly"]["time"] = [datetime.fromisoformat(x).replace(tzinfo=VIENNA).isoformat() for x in j["hourly"]["time"]]
    return j

@typechecked
def get_weather_data(j: Dict, d: date) -> Dict:
    """
    turn raw weather API json data to a dict of model input parameters.
    :return: e. g. {'temperature': 257.25, 'radiation': 3440.0, 'windspeed': 374.8, 'daylight_hours': 11.55}
    """
    d_iso = d.isoformat()
    #logger.debug(json.dumps(j["hourly"], indent=4))
    hourly_fields = ["temperature_2m", "shortwave_radiation", "windspeed_10m"]
    my_keys = ["temperature", "radiation", "windspeed"]
    param_dict_keys = ["temperature", "radiation", "windspeed", "daylight_hours"]
    param_dict = {f: 0.0 for f in param_dict_keys}
    hourly = get_dict(j["hourly"])
    #logger.debug(hourly)
    h = 0
    for k, v in {key: value for key, value in hourly.items() if key.startswith(d_iso)}.items():
        h += 1
        for i, f in enumerate(hourly_fields):
            param_dict[my_keys[i]] = param_dict[my_keys[i]] + v[f]
    # technically at least one hour is needed; more than 25 hours are suspicious
    if not 1 <= h <= 25:
        raise Exception("24 records expected but got {h}")
    if h != 24:
        logger.warn(f"not 24 but {h} records were delivered")
    # calculate average temperature, radiation and windspeed
    for k in my_keys:
        param_dict[k] = param_dict[k] / h

    #logger.debug(json.dumps(j["daily"], indent=4))
    daily = get_dict(j["daily"])
    #logger.debug(daily)
    sunrise = datetime.fromisoformat(daily[d_iso]["sunrise"])
    sunset = datetime.fromisoformat(daily[d_iso]["sunset"])
    daylight_hours = (sunset - sunrise).total_seconds() / 3600
    param_dict["daylight_hours"] = daylight_hours
    return param_dict



if __name__ == "__main__":
    today = date.today()
    tomorrow = today + timedelta(days=1)
    j = request_weather_data(today, tomorrow)
    data = get_weather_data(j, today)
    logger.debug(f"data of today ({today.isoformat()}):\n{data}")
    data = get_weather_data(j, tomorrow)
    logger.debug(f"data of tomorrow ({tomorrow.isoformat()}):\n{data}")
