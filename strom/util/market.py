import requests
from typing import List, Dict, Tuple, Union
from pydantic import BaseModel, RootModel, Field, computed_field, field_validator
from datetime import date, datetime, timedelta
from loguru import logger
from typeguard import typechecked
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from strom.model.market import MarketDataModel, SimpleMarketDataModel, MarketDataList, SimpleMarketDataList
from strom.util.misc import filter_dict
from strom.util.epoch import get_datetime_from_epoch, get_epoch_from_date, vienna_now
#from strom.util.epoch import get_midnight, get_epoch_from_datetime, get_datetime_from_epoch, vienna_now
from strom.util.const import H1_PRICE

##def get_start_end_epoch(vienna_now: datetime = None) -> (int, int):
#def get_start_end_epoch(as_of: date) -> (int, int):
#    """
#    Z. B.: 03.04.2023 14:18:32.324 -> epochs of (02.04.2023 00:00.000, 03.04.2023 00:00.000) -> (1680386400000, 1680472800000)
#    Z. B.: 03.04.2023 15:25:41.657 -> epochs of (03.04.2023 00:00.000, 04.04.2023 00:00.000) -> (1680472800000, 1680559200000)
#
#    """
#    vienna_now = vienna_now()
#    now = vienna_now.replace(minute=0, second=0, microsecond=0)
#    d1 = timedelta(days=-1 if now.hour < 14 else 0)
#    d2 = timedelta(days=2)
#    return (get_epoch_from_datetime(now + d1, ms=True), get_epoch_from_datetime(now + d1 + d2, ms=True))

@typechecked
def get_start_end_epoch(my_date: date) -> (int, int):
    """
    Z. B.: 01.04.2023 -> epochs of (02.04.2023 00:00.000, 03.04.2023 00:00.000) -> (1680386400000, 1680472800000)
    Z. B.: 02.04.2023 -> epochs of (03.04.2023 00:00.000, 04.04.2023 00:00.000) -> (1680472800000, 1680559200000)
    """
    #d1 = timedelta(days=-1 if dt.hour < 15 else 0)
    #d2 = timedelta(days=2)
    #return (get_epoch_from_datetime(get_midnight(dt + d1), ms=True), get_epoch_from_datetime(get_midnight(dt + d1 + d2), ms=True))
    return tuple([get_epoch_from_date(d=my_date + timedelta(days=tdelta), ms=True, vienna=True) for tdelta in [1, 2]])


@typechecked
def request_market_data(epoch_start: int, epoch_end: int) -> Dict:
    """
    Fragt das Strompreis-API ab und gibt die Daten als 1:1 von awattar
    :return: json
    """
    logger.info(f"epoch_start: {epoch_start} / epoch_end: {epoch_end}")
    response = requests.get(f"https://api.awattar.at/v1/marketdata?start={epoch_start}&end={epoch_end}")
    sc = response.status_code
    if sc != 200:
        raise Exception("No response from aWATTar")
    return response.json()


@typechecked
class MarketHandler():

    def __init__(self):
        self.md_dict = {}
        #self.md_list = MarketDataList()

    #def update(self, epoch: int):
    def update(self):
        try:
            vnow = vienna_now()
            logger.info(f"update MarketData at time: {vnow}")
            if vnow.hour >= 14:
                tdelta = timedelta(days=0)
            else:
                tdelta = timedelta(days=-1)

            dates = [vnow.date() + tdelta + timedelta(days=-1), vnow.date() + tdelta]

            md_dict = filter_dict(self.md_dict, dates)
            for my_date in dates:
                if my_date not in md_dict:
                    market_data = request_market_data(*get_start_end_epoch(my_date))
                    md_dict[my_date] = MarketDataList.model_validate(market_data["data"])
            self.md_dict = md_dict
        except:
            logger.error("update of MarketData failed")
            raise

    def get_simple_market_data_list(self, as_of: date = None, adjust: bool = True) -> SimpleMarketDataList:
        """
        Diese Funktion gibt die awattar-Daten als 2D-Array zurück
        :return: [[<hour>, <price>], [<hour>, <price>], ...]
        """
        market_data_list = []
        if as_of:
            # Take market data of one day
            market_data_list.extend(self.md_dict[as_of].root)
        else:
            # Take market data of all days
            for obj in self.md_dict.values():
                market_data_list.extend(obj.root)
        # sort market data by start time
        #market_data_list.sort(key=lambda obj: obj.start_timestamp)

        simple_market_data_list = SimpleMarketDataList.from_market_data_list(market_data_list)
        if adjust:
            simple_market_data_list = MarketHandler.adjust_simple_market_data_list(simple_market_data_list)

        #all = []
        #for obj in market_data_list:
        #    all.append((get_datetime_from_epoch(obj.start_timestamp, ms=True, vienna=True), obj.marketprice))
        #if adjust:
        #    all = MarketData.adjust_market_data(all)
        logger.info(f"return market_data_list: {simple_market_data_list.pstr()}")
        return simple_market_data_list

    def adjust_simple_market_data_list(simple_market_data_list: SimpleMarketDataList) -> SimpleMarketDataList:
        """
        Den Preis bei allen Stunden die nicht bezogen werden können auf den Tagstrompreis + 1 setzen.
        (Weil der Nachtstromzähler abgeschalten ist)
        :full_market_data: [[<hour>, <price>], [<hour>, <price>], ...]
        :return: [[<hour>, <price>], [<hour>, <price>], ...]
        """

        """
        Zeitfenster beim Nachstrom-Zähler
        Mo - So
        * 20:30 - 22:00
        * 23:00 - 07:30
        * variabel 2h zwischen 12:00 - 15:00
        """
        no_power_hours = [8, 9, 10, 11, 16, 17, 18, 19, 22]

        for item in simple_market_data_list:
            if item.start_dt.hour in no_power_hours:
                item.marketprice = H1_PRICE + 1 # EUR/MWh
        return simple_market_data_list

    def get_chart_market_data(self) -> List[List]:
        """
        Diese Funktion gibt alle awattar-Daten als 2D-Array zurück
        :return: [[<hour>, <price>], [<hour>, <price>], ...]
        """
        market_data_list = []
        # Iterate through the dictionary values and concatenate them
        for obj in self.md_dict.values():
            market_data_list.extend(obj.root)
        market_data_list.sort(key=lambda obj: obj.start_timestamp)

        all = []
        for obj in market_data_list:
            all.append([get_datetime_from_epoch(obj.start_timestamp, ms=True, vienna=True), obj.marketprice])
            all.append([get_datetime_from_epoch(obj.end_timestamp, ms=True, vienna=True), obj.marketprice])
        return all

if __name__ == "__main__":
    mh = MarketHandler()
    mh.update()
    logger.info(f"simple_market_data_list: {mh.get_simple_market_data_list().pstr()}")
