import io
from loguru import logger
import numpy as np
import random
from datetime import datetime, timedelta

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import matplotlib.dates as mdates
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

from strom.service.control import Action

# Stem plot
def create_prices(date_list, price_list, ns_price, now, schedule):
    #logger.debug(date_list)
    #logger.debug(now)
    fig, ax = plt.subplots(1)
    fig.set_size_inches(8, 4)
    fig.set_dpi(75)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d. %b %H:%M'))
    #plt.gca().xaxis.set_major_locator(mdates.HourLocator())
    ax.xaxis.set_major_locator(mdates.AutoDateLocator())
    fig.autofmt_xdate()
    # horizontal line on Nachtstrom-Preis
    plt.axhline(y=ns_price, color='r', linestyle='-')
    # vertical lines on midight
    for dt in date_list:
        if dt.hour == 0:
            plt.axvline(x=dt, color='g', linestyle='dashed')
    # vertical lines on current time
    plt.axvline(x=now, color='b', linestyle='-')
    plt.plot(date_list, price_list)
    ax.set_ylabel('Preis (EUR/MWh)', color='b')
    ax.yaxis.tick_right()
    # diagram starts at zero unless negative prices
    ax.set_ylim(ymin=min(min(price_list), 0))
    # background colors on heating intervals
    """
    on_intervals = [(h, h + timedelta(hours = 1)) for h in cheap_hours]
    for i in on_intervals:
        ax.axvspan(i[0], i[1], facecolor='orange', alpha=0.5) # on intervals
    """
    for ps in Action:
        for i in schedule.get_intervals(ps):
            ax.axvspan(i[0], i[1], facecolor=ps.color, alpha=0.5) # on intervals
    #return fig
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    output_value = output.getvalue()
    plt.clf()
    #plt.close("all")
    plt.close(fig)
    return output_value

def create_weather(hourly, now):
    hourly_fields = ["temperature_2m", "shortwave_radiation", "windspeed_10m"]
    label = ["Temperatur (°C)", "Sonne (W/m²)", "Wind (km/h)"]
    hourly_colors = ["purple", "orange", "blue"]

    prep_hourly = {}
    # span each time util next hour
    # [1, 2, 3] -> [1, 2, 2, 3, 3, 4]
    prep_hourly["time"] = [datetime.fromisoformat(h) + timedelta(hours = i) for h in hourly["time"] for i in range(2)]
    # each weather data point twice
    # [1, 2, 3] -> [1, 1, 2, 2, 3, 3]
    for f in hourly_fields:
        prep_hourly[f] = [h for h in hourly[f] for i in range(2)]
    prep_date_list = [h for h in prep_hourly["time"]]
    #logger.debug(prep_date_list)
    #logger.debug(now)

    #fig, ax = plt.subplots()
    fig, axs = plt.subplots(nrows=len(hourly_fields), sharex=True)
    fig.set_size_inches(8, 6)
    fig.set_dpi(75)
    axs[0].xaxis.set_major_formatter(mdates.DateFormatter('%d. %b %H:%M'))
    axs[0].xaxis.set_major_locator(mdates.AutoDateLocator())
    fig.autofmt_xdate()
    for ax in axs:
    # vertical lines on midight
        for dt in prep_date_list:
            if dt.hour == 0:
                ax.axvline(x=dt, color='g', linestyle='dashed')
    # vertical lines on current time
        ax.axvline(x=now, color='b', linestyle='-')
    # plot all weather data
    for i, f in enumerate(hourly_fields):
        #ax = ax.twinx()
        axs[i].plot(prep_date_list, prep_hourly[f], hourly_colors[i]) # plotting
        axs[i].set_ylabel(label[i], color=hourly_colors[i])
        axs[i].yaxis.tick_right()
    #ax.stem(x, y, bottom = 60)
    #return fig
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    output_value = output.getvalue()
    plt.clf()
    #plt.close("all")
    plt.close(fig)
    return output_value

# ToDo: delete?
#def create_figure():
#    fig = Figure()
#    axis = fig.add_subplot(1, 1, 1)
#    xs = range(100)
#    ys = [random.randint(1, 50) for x in xs]
#    axis.plot(xs, ys)
#    return fig
