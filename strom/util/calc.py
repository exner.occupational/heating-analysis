from operator import itemgetter
from typing import List, Tuple, Union
from datetime import datetime
from typeguard import typechecked
from loguru import logger
from strom.model.market import SimpleMarketDataModel, SimpleMarketDataList

dist_rules = { # hours in any slot, hours in slot 1, hours in slot 2
        1: (1, 0, 0),
        2: (2, 0, 0),
        3: (1, 1, 1),
        4: (2, 1, 1),
        5: (3, 1, 1),
        6: (2, 2, 2),
        7: (3, 2, 2),
        8: (4, 2, 2),
        9: (3, 3, 3),
        10: (4, 3, 3)}
slot_hours_1 = [23, 0, 1, 2, 3, 4, 5]
slot_hours_2 = [12, 13, 14, 15, 16, 17, 18]

def split_slot(market_data: list, slot_hours: list, slot_no: int):
    """
    move values from prices to slot if they are within slot_hours
    move values from prices to rest otherwise
    """
    rest, slot = [], []
    for x in market_data:
        slot.append(x) if x[0].hour in slot_hours and len(slot) < slot_no else rest.append(x)
    return  rest, slot

@typechecked
def calc_distribution(heating_time: int, simple_market_data_list: SimpleMarketDataList) -> List[Tuple[datetime, float]]:
    """
    heating_time: hours to heat
    market_data: list of (hour, price) 

    1: order by price, move to result
    2: move range1

    return: list of hours, e. g [0, 1, 2, 3, 16, 17]
    """
    market_data = [(smd.start_dt, smd.marketprice) for smd in simple_market_data_list]
    market_data_sorted = sorted(market_data, key=itemgetter(1)) # sort market_data by price
    (rest_no, slot_no_1, slot_no_2) = dist_rules[heating_time]
    rest, slot_1 = split_slot(market_data_sorted, slot_hours_1, slot_no_1)
    rest, slot_2 = split_slot(rest, slot_hours_2, slot_no_2)

    #return sorted([x[0] for x in rest[:rest_no] + slot_1[:slot_no_1] + slot_2[:slot_no_2]])
    return sorted(rest[:rest_no] + slot_1[:slot_no_1] + slot_2[:slot_no_2])

def calc_cheap_hours(heating_time: int, market_data: list):
    """
    Diese Funktion nimmt die übergebenen Daten (vom API) und gibt ein Array mit den
    je 2 'billigsten Stunden' innerhalb von zwei Intervallen und die billigsten Studen
    am restliche Tag, exklusive der bereits gewählen Zeiten.
    :param market_data: [[<hour>, <price>], [<hour>, <price>], ...]
    """
    logger.debug(f"heating_time: {heating_time}")
    logger.debug(f"market_data: {market_data}")
    (rest_no, slot_no_1, slot_no_2) = dist_rules[heating_time]

    # 2 zeilen mit geringstem preis in intervall finden
    # Problem, falls sich die Slots ueberschneiden?
    selected = sorted([x for x in market_data if x[0].hour in slot_hours_1], key=lambda x: x[1])[:slot_no_1]
    selected += sorted([x for x in market_data if x[0].hour in slot_hours_2], key=lambda x: x[1])[:slot_no_2]

    output = []
    output.extend(selected)

    # gewählte zeilen asu market_data array entfernen
    market_data_copy = market_data.copy()
    for row in selected:
        market_data_copy.remove(row)

    output.extend(sorted(market_data_copy, key=itemgetter(1))[:rest_no]) # den Rest nach Preis sortieren und die niedrigsten nehmen

    data = sorted(output, key=itemgetter(0)) # sort by hour
    print('Gewählte Stunden: ',data)
    return data
    #hours = sorted([x for (x, y) in output])
    #print(datetime.datetime.now(), ' > Gewählte Stunden: ', hours)
    #return hours
