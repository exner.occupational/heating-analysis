from typeguard import typechecked

@typechecked
def filter_dict(input_dict: dict, key_list: list):
    # Create a new dictionary with only the keys that exist in the key_list
    filtered_dict = {key: input_dict[key] for key in key_list if key in input_dict}
    return filtered_dict
