from loguru import logger
from typing import List, Dict, Tuple, Union
from datetime import datetime, timedelta
from flask_table import Table, Col, DatetimeCol
import urllib.parse
from typeguard import typechecked
from strom.service.control import Action
from strom.util.const import VIENNA, UTC
from strom.util.const import H1_PRICE

class ButtCol(Col):
    def td_format(self, content):
        return f"<input type=\"button\" onClick=\"deleteAction('{content}')\" />"

@typechecked
class ScheduleTable(Table):
    start = DatetimeCol('Start Time')
    end = DatetimeCol('End Timen')
    action = Col('Planned Action')
    button = ButtCol('Delete')

    def get_tr_attrs(self, item):
        return {'value': urllib.parse.quote_plus(item["start"].isoformat())}

@typechecked
class Planned:

    @typechecked
    def __init__(self, duration: timedelta, action: Action) -> None:
        if duration <= timedelta(0):
            raise Exception("Duration MUST be positive.")
        self.duration = duration
        self.action = action

@typechecked
class Schedule:
    """
    Zur Steuerung, wann im Tagesverlauf Nachtstrom oder Tagstrom oder kein Strom bezogen werden soll.
    """
    def __init__(self):
        self.intervals: Dict[datetime, Planned] = {}

    def get_action(self, dt: datetime) -> Action:
        """
        Gibt den RelayControl-Zustand zurück, der für den Zeitpunkt definiert ist.
        """
        keys = [i for i in self.intervals.keys() if i <= dt]
        if len(keys) > 0:
            key = max(keys)
            if key <= dt < key + self.intervals[key].duration:
                return self.intervals[key].action
        return Action.NS

    def intervals_between(self, dt1: datetime, dt2: datetime) -> (List[datetime], List[datetime]):
        """
        Liefert alle Keys für Intervalle deren Start- oder End-Zeitpunkte zwischen dt1 und dt2 liegt.
        :return: Liste aller Keys wo Start im Intervall, Liste aller Keys wo Ende im Intervall
        """
        starts = [i for i in self.intervals.keys() if dt1 <= i < dt2]
        ends = [i for i in self.intervals.keys() if dt1 < (i + self.intervals[i].duration) <= dt2]
        return (starts, ends)

    def del_intervals(self, to_remove: List[datetime]) -> None:
        for k in to_remove:
            if k in self.intervals:
                del self.intervals[k]
            else:
                logger.error(f"key {k} could not be removed from intevals")

    def del_intervals_from(self, start: datetime) -> None:
        """
        Delete all intervals before start

        :start: All intervals before this datetime will be deleted
        """
        na = len(self.intervals)
        self.intervals = {k: v for k, v in self.intervals.items() if k >= start}
        nb = len(self.intervals)
        logger.debug(f"deleted {nb - na} intervals")

    def del_interval(self, start: datetime) -> None:
        """
        Delete one interval

        :start: datetime of the interval to delete
        """
        self.del_intervals([start])

    def add_interval(self, dt: datetime, duration: timedelta, action: Action, override: bool) -> None:
        """
        Add one interval

        :dt: datetime, start of the interval
        :duration: timedelta, duration of the interval
        :action: what Action to do during the interval
        :override:
            If True, the interval will eventually override other intervals.
            If False, an exception will be raised when the interval overlaps with an existing interval.
        """
        #logger.debug(f"dt: {dt} / duration: {duration} / self: {str(self)}")
        logger.debug(f"dt: {dt} / duration: {duration}")
        (start_keys, end_keys) = self.intervals_between(dt, dt + duration)
        logger.debug(f"# overlapping starts / ends: {len(start_keys)} / {len(end_keys)}")
        if not override:
            if len(start_keys) == 0 and len(end_keys) == 0:
                self.intervals[dt] = Planned(duration, action)
                #self.intervals[dt] = (duration, action)
            else:
                raise Exception("interval already planned")
        else:
            dt1 = dt
            dt2 = dt + duration
            # alle Intervalle löschen, die komplett zwischen dt und dt + duration liegen
            keys = [i for i in self.intervals.keys() if dt1 <= i <= dt2 and dt1 <= (i + self.intervals[i].duration) <= dt2]
            self.del_intervals(keys)
            # alle Intervalle am Ende abschneiden, wo das Ende zwischen dt und dt + duration liegt
            keys = [i for i in self.intervals.keys() if dt1 < (i + self.intervals[i].duration) <= dt2]
            for k in keys:
                self.intervals[k] = Planned(dt1 - k, self.intervals[k].action)
            # alle Intervalle am Anfang abschneiden, wo der Beginn zwischen dt und dt + duration liegt
            keys = [i for i in self.intervals.keys() if dt1 <= i < dt2]
            for k in keys:
                self.intervals[dt2] = Planned(k + self.intervals[k].duration - dt2, self.intervals[k].action)
                self.del_interval(k)
            # alle Intervalle teilen, deren Ende und Anfang über dt und dt + duration hinausgehen
            keys = [i for i in self.intervals.keys() if i < dt1 and (i + self.intervals[i].duration) > dt2]
            for k in keys:
                self.intervals[dt2] = Planned(k + self.intervals[k].duration - dt2, self.intervals[k].action)
                self.intervals[k] = Planned(dt1 - k, self.intervals[k].action)
            # finally add new interval
            self.intervals[dt] = Planned(duration, action)

    def get_intervals(self, action: Action) -> List[Tuple[datetime, datetime]]:
        """
        Generate a sorted list of intervals of some planned action
        :action: result is filtered by action
        :return: sorted list of tuples (start, end)
        """
        s = dict(sorted(self.intervals.items()))
        return [(k, k + v.duration) for k, v in s.items() if v.action == action]

    @typechecked
    def calculate(self, cheap_hours: List[Tuple[datetime, Union[float, int]]]):
        """
        :cheap_hours: [[<hour>, <price>], [<hour>, <price>], ...]
        """
        now = datetime.now(VIENNA)
        tmp_dt = now.replace(hour=0, minute=0, second=0, microsecond=0)
        self.add_interval(tmp_dt, timedelta(days=2), Action.OFF, override=True)
        for item in cheap_hours:
            if item[1] == (H1_PRICE + 1):
                self.add_interval(item[0], timedelta(hours=1), Action.H1, override=True)
            else:
                self.add_interval(item[0], timedelta(hours=1), Action.NS, override=True)


    def get_html_table(self) -> str:
        """
        Provides input data for flask table object to render HTML
        """
        s = dict(sorted(self.intervals.items()))
        items = [{"start": k, "end": k + v.duration, "action": v.action, "button": k.isoformat()} for k, v in s.items()]
        # Populate the table
        table = ScheduleTable(items, border=True)
        # Return the HTML
        return table.__html__()

    def __str__(self):
        return ", ".join([f"{k.isoformat()}/{v.duration}/{v.action}" for k, v in self.intervals.items()])
