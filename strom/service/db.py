#import plyvel
from datetime import datetime
from dateutil.relativedelta import relativedelta
from plyvel import DB as plyvelDB
from loguru import logger
from typing import Dict, Union #, Tuple
from pydantic import BaseModel
from enum import Enum
#import pickle
from typeguard import typechecked
from strom.util.const import DB_FILE
from strom.util.epoch import vienna_now, get_epoch_from_datetime, assert_epoch
import traceback
#from loguru import logger

TENDAYS = relativedelta(days=-10, hour=0, minute=0, second=0, microsecond=0)

class DBDomain(Enum):
    AVAIL = b'a'
    PLAN = b'p'
    WEATHER = b'w'
    PRICE = b'p'

@typechecked
def _key_to_str(key: bytes) -> str:
    return f"{key[:1].decode()} + {DB._decode_key_epoch(key)}"

@typechecked
class DB():
    def __init__(self):
        logger.info(f"Open or create a LevelDB database: {DB_FILE}")
        self.db = plyvelDB(DB_FILE, create_if_missing=True)

    @staticmethod
    def _encode_key(dbdomain: DBDomain, epoch:int) -> bytes:
        """
        The key is composed of the DBDomain enum value and the epoch encoded as 8 bytes.

        :dbdomain: e. g. DBDomain.AVAIL
        :epoch: Unix epoch (no milis)
        :return: database key
        """
        encoded_key = dbdomain.value + epoch.to_bytes(8, 'big')
        #print(f"encoded key: {encoded_key}")
        return encoded_key

    @staticmethod
    def _decode_key_epoch(serialized_key: bytes) -> int:
        """
        Convert the encoded epoch within the database key to an integer.

        :key: bytes encoded key (DBDomain + epoch)
        :return: epoch as integer
        """
        return int.from_bytes(serialized_key[1:], 'big')

    #def put(domain: DBDomain, value: Union[Dict, Type[BaseModel]]):
    def put(self, dbdomain: DBDomain, epoch: int, value):
        """
        key has to be a Unix epoch
        value has to be boolean
        """
        assert_epoch(epoch, ms=False)
        serialized_key = DB._encode_key(dbdomain, epoch)
        if dbdomain in (DBDomain.AVAIL, DBDomain.PLAN):
            """
            value has to be boolean
            """
            assert type(value) == bool
            serialized_value = b'\x01' if value else b'\x00'
        #if domain == DBDomain.WEATHER:
        #    """
        #    value has to be a JSON
        #    """
        #    assert type(value) == int
        #    serialized_data = value.to_bytes(2, 'big')
        else:
            raise Exception("not implemented")
        self.db.put(serialized_key, serialized_value)

    def get(self, domain: DBDomain) -> Dict[int, object]:
        result = dict()

        # Iterate through the keys and values
        with self.db.iterator(prefix=domain.value) as db_it:
            for serialized_key, serialized_value in db_it:
                key = DB._decode_key_epoch(serialized_key)
                value = False if serialized_value == b'\x00' else True
                result[key] = value
            return result

    def cleanup(self, dt_delta: relativedelta = TENDAYS, epoch: Union[int, None] = None) -> int:
        """
        Remove older values that specified by dt_delta

        :dt_delta: timedelta
        :return: count of deleted records
        """
        count = 0
        if epoch is None:
            vnow = vienna_now()
            stop_epoch = get_epoch_from_datetime(vnow + dt_delta, ms=False)
        else:
            stop_epoch = epoch
        assert_epoch(stop_epoch, ms=False)
        for dbdomain in DBDomain:
            start = DB._encode_key(dbdomain, 0)
            stop = DB._encode_key(dbdomain, stop_epoch)
            #print(f"delete from {_key_to_str(start)} to {_key_to_str(stop)}")
            with self.db.iterator(start=start, stop=stop, include_value=False) as db_it:
                for serialized_key in db_it:
                    #print(f"delete key: {_key_to_str(serialized_key)}")
                    self.db.delete(serialized_key)
                    count += 1
        return count

    def __str__(self) -> str:
        result = "DB:\n"
        #with self.db.iterator() as db_it:
        #    for key, value in db_it:
        #        result = result + f"{key}: {value}\n"
        for key, value in self.db:
            result = result + f"{_key_to_str(key)}: {value}\n"
        return result


    def close(self):
        logger.info(f"Close LevelDB database: {DB_FILE}")
        try:
            self.db.close()
        except:
            pass

    #def __del__(self):
    #    logger.info(f"Close LevelDB database: {DB_FILE}")
    #    if hasattr(self, 'db'): # workaround
    #        self.db.close()
