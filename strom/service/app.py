import gc
#from guppy import hpy
from flask import Flask, request, render_template, Response
from threading import Thread
import requests
import time
from datetime import date, datetime, timedelta
from typing import List, Dict
from loguru import logger
import atexit
#from zoneinfo import ZoneInfo
#import io
import os
import psutil
import traceback
#import plyvel
from typeguard import typechecked
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from strom.util.const import VIENNA, UTC
from strom.util.epoch import get_midnight, get_epoch_from_datetime, get_datetime_from_epoch, vienna_now
from strom.util.graph import create_prices, create_weather
#from strom.util.market import request_market_data, get_market_data, get_chart_market_data, MarketHandler
from strom.util.market import MarketHandler
from strom.util.weather import request_weather_data, get_weather_data
from strom.util.calc import calc_cheap_hours
from strom.service.db import DB, DBDomain
from strom.service.control import RelayControl, Action, RelayState
from strom.service.forecast import eval_model, heating_time
from strom.service.schedule import Schedule
from strom.version import __version__ as version

from strom.util.const import TEMPLATE_FOLDER, LOG_FILE, PORT, H1_PRICE

class ExLogger:
    def __init__(self):
        self.tracked = set()
    #@staticmethod
    def _ex_to_str(self, ex: Exception):
        return self.tracked.add(str(type(ex)) + "".join(traceback.format_tb(ex.__traceback__)))
    def set_ex(self, ex: Exception):
        #if ExLogger._ex_to_str(ex) in self.tracked:
        if self._ex_to_str(ex) in self.tracked:
            logger.error(ex)
        else:
            logger.exception(ex)
            #self.tracked.add(ExTracker._ex_to_str(ex))
            self.tracked.add(self._ex_to_str(ex))
    def reset(self):
        self.tracked = set()

class RecalcSchedule:

    @staticmethod
    def get_applicable_date(vnow: datetime):
        if vnow.hour >= 14:
            return vnow.date()
        else:
            return vnow.date() - timedelta(hours=-1)

    def __init__(self):
        self.last = date.min

    def needed(self, vnow: datetime):
        next_date = RecalcSchedule.get_applicable_date(vnow)
        return self.last < next_date

    def done(self, vnow: datetime):
        self.last = RecalcSchedule.get_applicable_date(vnow)


#guppy_hpy = hpy()
#template_folder = '/home/pi/git/strom/templates'
#print(template_folder)
app = Flask(__name__, template_folder=TEMPLATE_FOLDER,)
process = psutil.Process(os.getpid())
db = None
logf = None
rc = RelayControl()
switch_state = False
prices_json = None
market_handler = MarketHandler()
weather_json = None
ex_logger = ExLogger()
cheap_hours = None
schedule = Schedule()
recalc_schedule = RecalcSchedule()
# Energiepreise "Nachtstrom Speicherheizung" ab 01.09.2022:
# 26.6675 inkl. 20% USt. und 6% Gebrauchsabgabe
# awattar bietet über das API Netto-Preise an.
# Daher 20% und 6% korrigieren sowie ca. 2 Cent weniger Nebenkosten abziehen.
# 26.6675 / 1.06 / 1.2 - 2 = 18.9650 Cent/kWh
rs: RelayState = None
ns_avail: bool = True


def get_start_end_epoch(dt: datetime) -> (int, int):
    """
    Z. B.: 03.04.2023 14:18:32.324 -> epochs of (02.04.2023 00:00.000, 03.04.2023 00:00.000) -> (1680386400000, 1680472800000)
    Z. B.: 03.04.2023 15:25:41.657 -> epochs of (03.04.2023 00:00.000, 04.04.2023 00:00.000) -> (1680472800000, 1680559200000)
    """
    d1 = timedelta(days=-1 if dt.hour < 15 else 0)
    d2 = timedelta(days=2)
    return (get_epoch_from_datetime(get_midnight(dt + d1), ms=True), get_epoch_from_datetime(get_midnight(dt + d1 + d2), ms=True))

def get_start_end_date(dt: datetime) -> (date, date):
    """
    Z. B.: 03.04.2023 14:18:32.324 -> 02.04.2023 03.04.2023
    Z. B.: 03.04.2023 15:25:41.657 -> 03.04.2023 04.04.2023
    """
    d1 = timedelta(days=-1 if dt.hour < 14 else 0)
    d2 = timedelta(days=1)
    return ((dt + d1).date(), (dt + d1 + d2).date())

def get_start_end_epoch_2(vienna_now: datetime) -> (int, int):
    """
    Z. B.: 03.04.2023 14:18:32.324 -> epochs of (02.04.2023 00:00.000, 03.04.2023 00:00.000) -> (1680386400000, 1680472800000)
    Z. B.: 03.04.2023 15:25:41.657 -> epochs of (03.04.2023 00:00.000, 04.04.2023 00:00.000) -> (1680472800000, 1680559200000)
    """
    now = vienna_now.replace(minute=0, second=0, microsecond=0)
    d1 = timedelta(days=-1 if now.hour < 14 else 0)
    d2 = timedelta(days=2)
    return (get_epoch_from_datetime(now + d1, ms=True), get_epoch_from_datetime(now + d1 + d2, ms=True))

#def get_start_end_date_2(vienna_now: datetime) -> (datetime, datetime):
#    """
#    Z. B.: 03.04.2023 14:18:32.324 -> 02.04.2023 03.04.2023
#    Z. B.: 03.04.2023 15:25:41.657 -> 03.04.2023 04.04.2023
#    """
#    now = vienna_now.replace(minute=0, second=0, microsecond=0)
#    d1 = timedelta(days=-1 if now.hour < 14 else 0)
#    d2 = timedelta(days=1)
#    return ((now + d1), (now + d1 + d2))

def planned_to_relay(action: Action, dt: datetime) -> RelayState:
    global market_handler
    global schedule
    logger.debug(f"action: {action} / dt: {dt.isoformat()}")
    if action == Action.OFF:
        return RelayState.OFF
    if action == Action.NS:
        return RelayState.NS
    if action == Action.H1:
        return RelayState.H1
    if action == Action.ANY:
        try:
            #my_market_data = get_market_data(prices_json)
            my_market_data = market_handler.get_market_data(adjust=True) # set no power hours to H1_PRICE + 1
            ns_price = my_market_data[dt.hour][1]
            logger.debug(f"ns_price: {ns_price} / H1_PRICE: {H1_PRICE}")
            if ns_price > H1_PRICE:
                return RelayState.H1
        except:
            logger.exception("error")
            pass
    return RelayState.NS

@typechecked
def check_time(vnow: datetime):
    """
    Wird jede Minute aufgerufen
    Prüft ob die jetztige Stunde im Array ist und ruft ggf die switch funktion auf
    um 15:00 werden neue Daten vom API aufgerufen
    """
    global prices_json
    global market_handler
    global weather_json
    global cheap_hours
    global schedule
    global recalc_schedule
    global ex_logger
    global rs

    if vnow.hour >= 14:
        tdelta = timedelta(days=0)
    else:
        tdelta = timedelta(days=-1)
    as_of = vnow.date() + tdelta

    market_handler.update()
    (epoch_start, epoch_end) = get_start_end_epoch_2(vnow)
    #epoch_current_hour = get_epoch_from_datetime(vnow.replace(minute=0, second=0, microsecond=0), ms=True)
    #d_current_hour = vnow.replace(minute=0, second=0, microsecond=0)
    (d_start, d_end) = get_start_end_date(vnow)
    del_from = datetime.combine(d_start, datetime.min.time(), VIENNA)
    logger.debug(f"d_start: {d_start}, d_end: {d_end}, del_from: {del_from}")
    schedule.del_intervals_from(del_from) # convert d_start to datetime
    logger.info(f"current time: {vnow} / epoch_start: {epoch_start} / epoch_end: {epoch_end}")
    if recalc_schedule.needed: # force recalculation of planned power switches
        ex_logger.reset()
        try:
            logger.info("get weather_json")
            weather_json = request_weather_data(d_start, d_end)
        except Exception as e:
            ex_logger.set_ex(e)
        #try:
        #    logger.info("get prices_json")
        #    # daten von API abfragen, gibt ein Array zurück: [[index, preis], ...]
        #    prices_json = request_market_data(epoch_start, epoch_end)
        #except Exception as e:
        #    ex_logger.set_ex(e)
        try:
            param_dict = get_weather_data(weather_json, d_end) # input data of model to calculate kwh
            kwh = eval_model(param_dict)
            logger.debug(f"kWh: {kwh}")
            smdl = market_handler.get_simple_market_data_list(as_of=as_of, adjust=True) # set no power hours to H1_PRICE + 1
            (cheap_hours, avg_cost) = heating_time(d_start, kwh, smdl)
            logger.info(f"cheap_hours = {cheap_hours}")
            logger.info(f"average cost = {avg_cost}")
        except Exception as e:
            ex_logger.set_ex(e)
        try:
            """
            Zeitfenster beim Nachstrom-Zähler
            Mo - So
            * 20:30 - 22:00
            * 23:00 - 07:30
            * variabel 2h zwischen 12:00 - 15:00
            """
            logger.info("recalculating schedule")
            schedule.calculate(cheap_hours)
            recalc_schedule.done(vnow)
        except Exception as e:
            ex_logger.set_ex(e)

        """
        # Winterbetrieb
        # Tagstrom zwischen 17:00 und 17:30
        tmp_dt = vnow.replace(hour=17, minute=00, second=0, microsecond=0)
        schedule.add_interval(tmp_dt, timedelta(minutes=30), Action.H1, override=True)
        # Off zwischen nächsten Tag 00:00 und 04:00
        tmp_dt = vnow.replace(hour=0, minute=0, second=0, microsecond=0)
        schedule.add_interval(tmp_dt + timedelta(days=1), timedelta(hours=4), Action.OFF, override=True)
        # Off zwischen nächsten Tag 06:00 und 11:00
        tmp_dt = vnow.replace(hour=6, minute=0, second=0, microsecond=0)
        schedule.add_interval(tmp_dt + timedelta(days=1), timedelta(hours=5), Action.OFF, override=True)
        """

        """
        # Sommerbetrieb
        # Von heute 00:00 zwei Tage abschalten
        tmp_dt = vnow.replace(hour=0, minute=0, second=0, microsecond=0)
        schedule.add_interval(tmp_dt, timedelta(days=2), Action.OFF, override=True)
        # Gleicher Tag 20:30 - 21:00
        tmp_dt = vnow.replace(hour=20, minute=30, second=0, microsecond=0)
        schedule.add_interval(tmp_dt, timedelta(minutes=30), Action.NS, override=True)
        # Nächster Tag 05:30 und 06:00
        tmp_dt = vnow.replace(hour=5, minute=30, second=0, microsecond=0)
        schedule.add_interval(tmp_dt + timedelta(days=1), timedelta(minutes=30), Action.NS, override=True)
        """

    action = schedule.get_action(vnow)
    rs = planned_to_relay(action, vnow)
    logger.debug(f"rs: {rs}")
    rc.set_state(rs)
    """
    if vnow.hour in [dt.hour for dt in cheap_hours]:
        rc.h1()
    else:
        rc.ns()
    """

def collect():
    logger.debug("Collecting...")
    n = gc.collect()
    #logger.debug(f"Number of unreachable objects collected by GC: {n}")
    #logger.debug(f"Uncollectable garbage: {gc.garbage}")

def main_loop():
    global db
    global logf
    global rs
    global ns_avail
    logger.debug(f"open file: {LOG_FILE}")
    logf = open(LOG_FILE, "a+")
    db = DB()

    while True:
        try:
            logger.info(f"start of main loop iteration, mem usage: {process.memory_info().rss}")
            #logger.info(guppy_hpy.heap())
            (rs, ns_avail) = rc.get_state()
            vnow = vienna_now()
            epoch = get_epoch_from_datetime(vnow, ms=True)
            db.put(DBDomain.AVAIL, epoch, ns_avail)
            vnow_str = vnow.astimezone(VIENNA).strftime("%d.%m.%Y %H:%M")
            logf.write(f"{vnow_str},{int(ns_avail)}\n")
            logf.flush()
            check_time(vnow)
            collect()
            next_time = (vnow + timedelta(minutes=1)).replace(second=0, microsecond=0)
            # Berechnet die Zeit bis zur nächsten vollen Minute
            time_to_sleep = (next_time - vnow).total_seconds() + 1 # add one second
        except:
            logger.exception("main loop iteration went wrong. waiting 15 seconds and try again")
            time_to_sleep = 15 # 15 seconds
        # wartet bis zur berechneten Zeit
        time.sleep(time_to_sleep)

def cleanup():
    global db
    global logf
    # Release your allocated resources here
    logger.info("free resources")
    db.close()
    logf.close()

def start_counter_thread():
    # Register the cleanup function
    atexit.register(cleanup)
    counter_thread = Thread(target=main_loop, daemon=True)
    counter_thread.start()

@app.route('/prices.png')
def prices_png():
    global market_handler
    #global prices_json
    global cheap_hours
    global schedule

    #if prices_json:
    if market_handler:
        prices = market_handler.get_chart_market_data()
        date_list, price_list = zip(*prices)
        logger.debug(f"# dates: {len(date_list)} / # prices: {len(prices) / 2} / H1_PRICE: {H1_PRICE} / schedule: {schedule}")
        output_value = create_prices(date_list, price_list, H1_PRICE, vienna_now(), schedule)
        return Response(output_value, mimetype='image/png')
    else:
        return "price data not available"

@app.route('/weather.png')
def weather_png():
    global weather_json
    if weather_json:
        hourly = weather_json["hourly"]
        output_value = create_weather(hourly, vienna_now())
        return Response(output_value, mimetype='image/png')
    else:
        return "weather data not available"

@app.route('/state.html')
def state_html():
    global rc
    (rs, ns_avail) = rc.get_state()
    #logger.debug(f"NS: {ns_avail}")
    relay_state = rs.name if rs else "n/a"
    avail_str = str(ns_avail) if ns_avail else "n/a"
    return f"<p>Schaltung: {relay_state} / Nachtstrom aktiv: {avail_str} / Mem: {process.memory_info().rss:,d} / Version: {version}</p>"

@app.route('/schedule.html')
def schedule_html():
    global schedule
    return schedule.get_html_table()

@app.route('/schedule_add', methods = ['POST'])
def schedule_add():
    try:
        global schedule
        j = request.get_json()
        logger.debug(j)
        logger.debug(j["state"])
        action = Action[j["state"]]
        start = datetime.fromisoformat(j["start"]).replace(tzinfo=VIENNA)
        duration = datetime.fromisoformat(j["end"]).replace(tzinfo=VIENNA) - start
        schedule.add_interval(start, duration, action, override=True)
        logger.debug(f"schedule: {schedule}")
        return "ok"
    except Exception as e:
        return str(e)

@app.route('/delete_action', methods = ['DELETE'])
def delete_action():
    global schedule
    start_iso = request.args.get('start_iso')
    logger.debug(f"delete from schedule: {start_iso}")
    schedule.del_interval(datetime.fromisoformat(start_iso))
    return "ok"

@app.route('/set_state', methods = ['POST'])
def set_state():
    try:
        j = request.get_json()
        logger.debug(j)
        state = RelayState[j["state"]]
        rc.set_state(state)
        return "done"
    except:
        return "failure"

#@app.route('/prices')
#def get_prices():
#    global prices_json
#    return prices_json

@app.route('/weather')
def get_weather():
    global weather_json
    return weather_json

@app.route('/cheap_hours')
def get_cheap_hours():
    global cheap_hours
    return cheap_hours or "None"

@app.route('/')
def get_main():
    return render_template('index.html', init_state=state_html())

@app.route('/switch')
def switch_switch():
    global switch_state
    switch_state = False if switch_state else True
    logger.debug(f"switch_state: {switch_state}")
    return 'ok'

@app.route('/avail')
def get_avail():
    global db
    data = db.get(DBDomain.AVAIL)
    logger.debug(f"avail size: {len(data)}")
    return data


if __name__ == "__main__":
    try:
        start_counter_thread()
        app.debug = True
        app.run(host='0.0.0.0', port=PORT, processes=1, threaded=False, use_reloader=False)
    finally:
        rc = None
        logger.info("shutdown message")
