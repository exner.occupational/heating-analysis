from datetime import datetime, date, timedelta
from typing import List, Tuple, Union
from typeguard import typechecked
from operator import itemgetter
import requests
import json
from strom.model.market import SimpleMarketDataModel, SimpleMarketDataList
from strom.util.calc import calc_distribution

model_dict = {'temperature': -77.20352511166335, 'radiation': 2.7808968681494264, 'windspeed': 5.578920054492248, 'daylight_hours': -11.8095589532916, 'const': 115.56444552270315, 'min': {'temperature': -1.4917317708333335, 'radiation': 41.608539094650205, 'windspeed': 8.141008771929824, 'daylight_hours': 8.49654761904762}, 'max': {'temperature': 15.310912698412695, 'radiation': 228.96929824561403, 'windspeed': 16.41022012578616, 'daylight_hours': 13.581228070175436}}
month_winter = [9, 10, 11, 12, 1, 2, 3, 4, 5]
tomorrow = datetime.today() + timedelta(days=1)
tomorrow_iso = tomorrow.date().isoformat()
param_dict_keys = ["temperature", "radiation", "windspeed", "daylight_hours"]

@typechecked
def heating_time(d: date, kwh: Union[int, float], simple_market_data_list: SimpleMarketDataList) -> Tuple[List[Tuple[datetime, float]], float]:
    """
    d      .. date
    kwh    .. needed power
    prices .. list of prices to select, e. g. [(0, 3.14), (1, 5.33), (2, 7.55), (3, 9.77), (4, 4.56), ...]
    heating_period:   0 kWh: 0h
    heating_period: 120 kWh: 6h
    additional 1h in any case (cellar, hot water)

    return: list of hours, e. g [0, 1, 2, 3, 16, 17] and mean cost
    """
    assert isinstance(d, date)
    winter = True if d.month in month_winter else False
    heating_time = int(max(kwh * 0.05, 0) + 1 + 0.5) if winter else 1 # winter + boiler + zur kaufmännischen rundung
    #heating_time = max(heating_time, 1)
    heating_time = max(heating_time, 2)
    heating_time = min(heating_time, 10)
    distribution = calc_distribution(heating_time, simple_market_data_list)
    #print(distribution)
    avg_cost = sum(x[1] for x in distribution) / len(distribution)
    #return ([x[0] for x in distribution], avg_cost)
    print(f"distribution: {distribution}")
    return (distribution, avg_cost)

def eval_model(param_dict, summer_season: bool = False) -> int:
    """
    param_dict: {"temperature": <C>, "radiation": <W/m2> ,"windspeed": <km/h>, "daylight_hours": <h>}
    return: kWh
    """
    #print(f"param_dict: {param_dict}")
    # normalization
    normalized_dict = {}
    for f in param_dict_keys:
        normalized_dict[f] = (param_dict[f] - model_dict["min"][f]) / (model_dict["max"][f] - model_dict["min"][f])

    kwh = model_dict["temperature"] * normalized_dict["temperature"] + \
          model_dict["radiation"] * normalized_dict["radiation"] + \
          model_dict["windspeed"] * normalized_dict["windspeed"] + \
          model_dict["daylight_hours"] * normalized_dict["daylight_hours"] + \
          model_dict["const"]
    #print(f"kWh: {kwh}")

    kwh = 12 if summer_season else kwh # Annahme: Warmwasser und Keller benötigen 12 kWh pro Tag Winter wie Sommer
    return kwh

if __name__ == "__main__":

    from strom.util.weather import request_weather_data, get_weather_data

    today = date.today()
    tomorrow = today + timedelta(days=1)

    print(f"\ntomorrow")
    #param_dict = get_weather_data(request_weather_data(tomorrow, tomorrow), tomorrow)
    #kwh = eval_model(param_dict)
    #print(f"param_dict: {param_dict} --> {kwh} kWh")

    data = [
    ("tomorrow", get_weather_data(request_weather_data(tomorrow, tomorrow), tomorrow)),
    ("-10 Grad mit Wind ohne Sonne", {'radiation': 0, 'windspeed': 30, 'temperature': -10, 'daylight_hours': 8.45}),
    ("-10 Grad ohne Wind mit Sonne", {'radiation': 80, 'windspeed': 0, 'temperature': -10, 'daylight_hours': 8.45}),
    ("0 Grad mit Wind ohne Sonne", {'radiation': 0, 'windspeed': 30, 'temperature': 0, 'daylight_hours': 8.45}),
    ("0 Grad ohne Wind mit Sonne", {'radiation': 80, 'windspeed': 0, 'temperature': 0, 'daylight_hours': 8.45}),
    ("10 Grad mit Wind ohne Sonne", {'radiation': 20, 'windspeed': 30, 'temperature': 10, 'daylight_hours': 8.45}),
    ("10 Grad ohne Wind mit Sonne", {'radiation': 80, 'windspeed': 0, 'temperature': 10, 'daylight_hours': 8.45}),
    ("20 Grad mit Wind ohne Sonne", {'radiation': 20, 'windspeed': 30, 'temperature': 20, 'daylight_hours': 8.45}),
    ("20 Grad ohne Wind mit Sonne", {'radiation': 80, 'windspeed': 0, 'temperature': 20, 'daylight_hours': 8.45})
    ]

    for d in data:
        param_dict = d[1]
        kwh = eval_model(param_dict)
        print(d[0])
        print(f"param_dict: {param_dict} --> {kwh} kWh")

    '''
    consumption  date_from    date_to heating_period  diff_days  temperature  radiation  windspeed  daylight_hours
    1060.2 2019-10-12 2019-10-31              w       19.0       5273.6    43026.0    1031.47      202.001389
    3048.6 2019-10-31 2019-12-06              w       36.0       5183.8    40773.0    3410.63      333.302778
     763.3 2019-12-06 2019-12-13              w        7.0        151.1     7321.0     445.50       59.475833
    4662.2 2019-12-13 2020-01-29              w       47.0       1829.5    49766.0    3307.56      404.069722
    '''
    #print()
    #for x in [
    #        {'consumption': 1060, 'diff_days': 19, 'radiation': 43026, 'windspeed': 1031, 'temperature': 5273, 'daylight_hours': 202},
    #        {'consumption': 3048, 'diff_days': 36, 'radiation': 40773, 'windspeed': 3410, 'temperature': 5183, 'daylight_hours': 333},
    #        {'consumption': 763,  'diff_days': 7,  'radiation': 7321,  'windspeed': 445,  'temperature': 151,  'daylight_hours': 59},
    #        {'consumption': 4662, 'diff_days': 47, 'radiation': 49766, 'windspeed': 3307, 'temperature': 1829, 'daylight_hours': 404}]:
    #    diff_days = x["diff_days"]
    #    y = {k: int(v / diff_days) for k, v in x.items()}
    #    print(y)
    #    ,y)
    #    print()
