try:
    import RPi.GPIO as GPIO
except:
    import Mock.GPIO as GPIO
from typeguard import typechecked
from time import sleep
from enum import Enum
from typing import Tuple
#import random

# https://stackoverflow.com/questions/12680080/python-enums-with-attributes
class Action(Enum):

    def __new__(cls, *args, **kwds):
        obj = object.__new__(cls)
        obj._value_ = args[0]
        return obj

    # ignore the first param since it's already set by __new__
    def __init__(self, _: str, color):
        self._color_ = color

    # this makes sure that the color is read-only
    @property
    def color(self):
        return self._color_

    OFF = 1, "grey"
    NS = 2, "blue"
    H1 = 3, "green"
    ANY = 4, "red"

class RelayState(Enum):
    FAULTY = 0
    OFF = 1
    NS = 2
    H1 = 3

@typechecked
class RelayControl():
    H1 = 23 # Tagstrom Relais (Tagstrom an, wenn Relais an)
    NS = 25 # Nachtstrom Relais (Nachtstrom an, wenn Relais aus)
    IN = 13 # Eingang um zu messen, ob Nachtstrom vorhanden

    def __init__(self):
        #GPIO.cleanup()
        GPIO.setmode(GPIO.BCM) # choose BCM or BOARD
        GPIO.setup(RelayControl.H1, GPIO.OUT, initial=GPIO.LOW) # LOW .. Tagstrom aus
        GPIO.setup(RelayControl.NS, GPIO.OUT, initial=GPIO.LOW) # LOW .. Nachtstrom ein
        GPIO.setup(RelayControl.IN, GPIO.IN) # do not configure pull up / pull down
        #GPIO.setup(RelayControl.IN, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) # does not work?

    def __del__(self):
        self.ns()
        GPIO.cleanup()

    def h1(self):
        GPIO.output(RelayControl.NS, GPIO.HIGH) # Nachtstrom aus
        sleep(0.1)
        GPIO.output(RelayControl.H1, GPIO.HIGH) # Tagstrom ein

    def ns(self):
        GPIO.output(RelayControl.H1, GPIO.LOW) # Tagstrom aus
        sleep(0.1)
        GPIO.output(RelayControl.NS, GPIO.LOW) # Nachtstrom ein

    def off(self):
        GPIO.output(RelayControl.NS, GPIO.HIGH) # Nachtstrom aus
        GPIO.output(RelayControl.H1, GPIO.LOW) # Tagstrom aus

    def set_state(self, rs: RelayState) -> None:
        if rs == RelayState.NS:
            self.ns()
        elif rs == RelayState.H1:
            self.h1()
        else:
            self.off()

    def get_state(self) -> Tuple[RelayState, bool]:
        rs = RelayState.FAULTY
        h1_state = GPIO.input(RelayControl.H1)
        ns_state = GPIO.input(RelayControl.NS)
        # ToDo (hardware + software): Check if Nachtstrom is available
        #ns_avail = random.choice([True, False])
        ns_avail = (GPIO.input(RelayControl.IN) != GPIO.HIGH)
        if h1_state == GPIO.LOW and ns_state == GPIO.LOW: rs = RelayState.NS
        if h1_state == GPIO.HIGH and ns_state == GPIO.HIGH: rs = RelayState.H1
        if h1_state == GPIO.LOW and ns_state == GPIO.HIGH: rs = RelayState.OFF
        return (rs, ns_avail)
