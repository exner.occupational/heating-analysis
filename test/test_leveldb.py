import plyvel

def test_2():
    # Open or create a LevelDB database
    db = plyvel.DB('/tmp/test.db', create_if_missing=True)

    # Put data into the database
    db.put(b'key1', b'value1')
    db.put(b'key2', b'value2')

    # Get data from the database
    value = db.get(b'key1')
    print(value.decode('utf-8'))  # Decode bytes to string

    # Iterate through all key-value pairs
    for key, value in db:
        print(key.decode('utf-8'), value.decode('utf-8'))

    # Close the database when done
    db.close()
