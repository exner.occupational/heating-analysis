import pytest

#from strom.service.db import DB, DB_FILE
from strom.service.db import DB

@pytest.fixture(scope="function")
def db(mocker):
    mocker.patch("strom.service.db.DB_FILE", "/tmp/mocked.db")
    db = DB()
    return db
