import pytest
from datetime import datetime, timedelta
from strom.service.schedule import Schedule
from strom.service.control import Action

dt1 = datetime.fromisoformat('2023-03-12T16:00:00')
td1 = timedelta(hours=2)

dt2 = datetime.fromisoformat('2023-03-12T18:00:00')
td2 = timedelta(hours=1)

dt3 = datetime.fromisoformat('2023-03-12T18:30:00')
td3 = timedelta(hours=1)

dt4 = datetime.fromisoformat('2023-03-12T15:30:00')
td4 = timedelta(hours=4)

def test_action_1():
    ch = Schedule()
    assert ch.get_action(dt1) == Action.NS

def test_add_intervals_1():
    ch = Schedule()
    ch.add_interval(dt1, td1, Action.H1, override=False)
    assert ch.get_action(dt1) == Action.H1
    assert ch.get_action(dt2) == Action.NS

def test_add_intervals_2():
    ch = Schedule()
    ch.add_interval(dt2, td2, Action.H1, override=False)
    with pytest.raises(Exception):
        ch.add_interval(dt3, td3, Action.H1, override=False)
    ch.add_interval(dt3, td3, Action.ANY, override=True)
    assert ch.get_intervals(Action.NS) == []
    assert ch.get_intervals(Action.H1) == [(dt2, dt3)]
    assert ch.get_intervals(Action.ANY) == [(dt3, dt3 + td3)]

def test_add_intervals_3():
    ch = Schedule()
    ch.add_interval(dt4, td4, Action.H1, override=False)
    ch.add_interval(dt1, td1, Action.NS, override=True)
    assert ch.get_intervals(Action.H1) == [(dt4, dt1), (dt1 + td1, dt4 + td4)]
    assert ch.get_intervals(Action.NS) == [(dt1, dt1 + td1)]

def test_del_intervals_from_1():
    ch = Schedule()
    ch.add_interval(dt1, td1, Action.H1, override=False)
    ch.add_interval(dt2, td2, Action.NS, override=False)
    ch.add_interval(dt3, td3, Action.OFF, override=True)
    assert len(ch.intervals) == 3
    ch.del_intervals_from(dt2)
    assert len(ch.intervals) == 2
    ch.del_intervals_from(dt3)
    assert len(ch.intervals) == 1
