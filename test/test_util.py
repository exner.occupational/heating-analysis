#from ..util import epoch
from strom.util.epoch import get_epoch_from_datetime, get_epoch_from_date, get_datetime_from_epoch, get_batches
from strom.util.const import VIENNA, UTC
from datetime import datetime, date
import pytest

def test_get_epoch_from_datetime_1():
    # Liefert Strompreisdaten vom 01.07.2019:
    # curl "https://api.awattar.at/v1/marketdata?start=1561939200000"

    dt = datetime(2019, 7, 1, 0, 0, 0, tzinfo=UTC)
    epoch = get_epoch_from_datetime(dt, ms=True)
    assert epoch == 1561939200000

def test_get_epoch_from_datetime_2():
    dt = datetime(2019, 7, 1, 0, 0, 0, tzinfo=VIENNA)
    epoch = get_epoch_from_datetime(dt, ms=True)
    assert epoch == 1561932000000

def test_get_epoch_from_datetime_3():
    dt = datetime(2019, 7, 1, 0, 0, 0)
    with pytest.raises(Exception):
        epoch = get_epoch_from_datetime(dt, ms=True)

def test_get_epoch_from_date_1():
    d = date(2019, 7, 1)
    epoch = get_epoch_from_date(d, ms=True)
    assert epoch == 1561939200000

def test_get_epoch_from_date_2():
    d = date(2022, 12, 29)
    epoch = get_epoch_from_date(d, ms=True, vienna=True)
    assert epoch == 1672268400000

def test_get_datetime_from_epoch_1():
    dt = get_datetime_from_epoch(1561939200000, ms=True)
    assert dt == datetime(2019, 7, 1, 0, 0, 0, tzinfo=UTC)

def test_get_datetime_from_epoch_2():
    # dt = datetime(2019, 7, 1, 0, 0, 0, tzinfo=vienna)
    # dt.timestamp()
    # 1561935300.0
    dt = get_datetime_from_epoch(1561932000000, ms=True, vienna=True)
    assert dt == datetime(2019, 7, 1, 0, 0, 0, tzinfo=VIENNA)

def test_get_batches_1():
    l = get_batches(3, date(2022, 1, 1), date(2022, 1, 7))
    assert l == [
            (date(2022, 1, 1), date(2022, 1, 4)),
            (date(2022, 1, 4), date(2022, 1, 7)),
            ]

def test_get_batches_2():
    l = get_batches(3, date(2022, 1, 2), date(2022, 1, 7))
    assert l == [
            (date(2022, 1, 2), date(2022, 1, 5)),
            (date(2022, 1, 5), date(2022, 1, 7)),
            ]
