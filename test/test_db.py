import pytest
from dateutil.relativedelta import relativedelta
from strom.service.db import DBDomain, DB
from strom.util.epoch import vienna_now, get_epoch_from_datetime


def dbg(name, db):
    print(" * ".join([name for _ in range(10)]))
    print(str(db))

def test_1(db):
    #dbg("test_1", db)
    count = db.cleanup(relativedelta())
    #print(f"count: {count}")
    dbg("test_1", db)
    db.close()

def test_2(db):
    #dbg("test_2", db)
    vnow = vienna_now()
    epoch = get_epoch_from_datetime(vnow, ms=False)
    #db = DB()
    db.put(DBDomain.AVAIL, epoch - 1, True)
    db.put(DBDomain.AVAIL, epoch - 2, False)
    storage = db.get(DBDomain.AVAIL)
    assert storage[epoch - 1] == True
    assert storage[epoch - 2] == False
    #dbg("test_2", db)
    db.close()

def test_3(db):
    #dbg("test_3", db)
    count = db.cleanup(relativedelta())
    #dbg("test_3", db)
    assert count == 2
    db.close()

def test_4(db):
    vnow = vienna_now()
    epoch = get_epoch_from_datetime(vnow, ms=False)
    #db = DB()

    XDAYS = relativedelta(days=-40, hour=0, minute=0, second=0, microsecond=0)
    vnow = vienna_now()
    dt = vnow + XDAYS
    epoch = get_epoch_from_datetime(vnow, ms=False)

    db.put(DBDomain.AVAIL, epoch - 1, True)
    db.put(DBDomain.AVAIL, epoch, True)
    db.put(DBDomain.AVAIL, epoch + 1, False)
    count = db.cleanup(relativedelta(), epoch)
    assert count == 1
    storage = db.get(DBDomain.AVAIL)
    assert storage[epoch] == True
    assert storage[epoch + 1] == False
    #assert db.get(DB._encode_key(DBDomain.AVAIL, epoch)) == True
    #assert db.get(DB._encode_key(DBDomain.AVAIL, epoch + 1)) == False
    db.close()


def test_exc_1():
    with pytest.raises(Exception) as exc:
        vnow = vienna_now()
        epoch = get_epoch_from_datetime(vnow, ms=True)
        db = DB()
        db.put(DBDomain.AVAIL, epoch, True)
        db.close()

