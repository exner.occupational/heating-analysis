from datetime import datetime, date
from strom.util.const import VIENNA, UTC
from strom.service.app import get_start_end_epoch


def test_get_start_end_1():
    """
    Z. B.: 03.04.2023 14:18:32.324 -> epochs of (02.04.2023 00:00.000, 03.04.2023 00:00.000) -> (1680386400000, 1680472800000)
    Z. B.: 03.04.2023 15:25:41.657 -> epochs of (03.04.2023 00:00.000, 04.04.2023 00:00.000) -> (1680472800000, 1680559200000)
    """

    dt = datetime(2023, 4, 3, 14, 18, 32, 324, tzinfo=VIENNA)
    (s, e) = get_start_end_epoch(dt)
    assert (s, e) == (1680386400000, 1680559200000)

    dt = datetime(2023, 4, 3, 15, 25, 41, 657, tzinfo=VIENNA)
    (s, e) = get_start_end_epoch(dt)
    assert (s, e) == (1680472800000, 1680645600000)
