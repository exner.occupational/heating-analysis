# Virtual Environments
alias vha=". ~/virtualenvs/heating_analysis/bin/activate"
alias vxx='deactivate && rm -r ~/virtualenvs/heating_analysis && mkdir ~/virtualenvs/heating_analysis && python3 -m venv ~/virtualenvs/heating_analysis && . ~/virtualenvs/heating_analysis/bin/activate && pip install pip-tools'
#alias vha=". ~/virtualenvs/heating-analysis/bin/activate && cd ~/git/heating-analysis/"
#alias vse=". ~/virtualenvs/service/bin/activate"

# Development
alias l='python -m licenseheaders -E py html -x setup.py version.py -n heating_analysis -y 2024 -t ~/git/heating_analysis/.gpl-v3.tmpl -o "Alexander Exner" -u "https://gitlab.com/exner.occupational/heating_analysis"'
alias pi='pip install --editable .'
alias pc='pip-compile -r requirements.in'

# Test
alias s="jupyter-notebook --notebook-dir ~/git/heating_analysis/notebooks"

# Remote Actions
alias pd='ssh pi@strom "cd /home/pi/git/heating_data && git add * && git commit -m updateDBdata && git push"' # push data
alias gd='pushd ~/git/heating_data && git pull && popd' # get data

# Misc
# list git controlled files (within current directory) as tree
alias tg='tree --gitignore --prune --noreport --sort=version'
# list python files (within project directory) as tree, omit pytest files
alias tp='tree -P "*.py" -I "test*" --prune --noreport --sort=version ~/git/strom/strom'
